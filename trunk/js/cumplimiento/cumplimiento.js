$(document).ready(function() {

    var base_url = $('input[name=base_url]').val();
    
    // Eliminación de un proceso
    $('a.procesos').click(function() {
        var proceso_id = $(this).attr('id');
        var proceso_titulo = $('#titulo_' + proceso_id).text();
        $('#modal-eliminar-proceso-titulo').html(proceso_titulo);
        $('#modal-btn-aceptar').html('<a href="' + base_url + 'procesos/delete/' + proceso_id + '" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-proceso').modal('show');
    });

    // Eliminación de un Subproceso
    $('a.subprocesos').click(function() {
        var subproceso_id = $(this).attr('id');
        var proceso_id = $( '#proceso_'+subproceso_id ).val();
        var subproceso_titulo = $('#titulo_' + subproceso_id).text();
        $('#modal-eliminar-subproceso-titulo').html(subproceso_titulo);
        $('#modal-btn-aceptar').html('<a href="' + base_url + 'subprocesos/delete/'+ proceso_id +'/'+ subproceso_id +'" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-subproceso').modal('show');
    });

    // Eliminación de una norma
    $('a.normas').click(function() {
        var proceso_id = $('#proceso_id').val();
        var norma_id = $(this).attr('id');
        var subproceso_id = $('#subproceso_'+ norma_id).val();
        var norma_titulo = $('#titulo_' + norma_id).text();
        $('#modal-eliminar-norma-titulo').html(norma_titulo);
        $('#modal-btn-aceptar').html('<a href="' + base_url + 'normas/delete/' + proceso_id + '/' + subproceso_id + '/' + norma_id + '" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-norma').modal('show');
    });

    // Eliminación de un documento de normas
    $('a.doc-norma').click(function() {
        var norma_id = $(this).attr('id');
        var proceso_id = $('input[name=proceso_id]').val();
        var subproceso_id = $('input[name=subproceso_id]').val();
        $('#modal-btn-aceptar').html('<a href="' + base_url + 'normas/edit/' + proceso_id + '/' + subproceso_id + '/' + norma_id + '/del" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-doc-norma').modal('show');
    });


    // Eliminación de un procedimiento
    $('a.procedimientos').click(function() {
        var proceso_id = $('input[name=proceso_id]').val();
        var subproceso_id = $('input[name=subproceso_id]').val();
        var procedimiento_id = $(this).attr('id');
        var procedimiento_titulo = $('#titulo_' + procedimiento_id).text();
        $('#modal-eliminar-procedimiento-titulo').html(procedimiento_titulo);
        $('#modal-btn-aceptar-procedimiento').html('<a href="' + base_url + 'procedimientos/delete/' + proceso_id + '/' + subproceso_id + '/' + procedimiento_id + '" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-procedimiento').modal('show');
    });

    // Eliminación de un documento de procedimientos
    $('a.doc-procedimiento').click(function() {
        var proceso_id = $('input[name=proceso_id]').val();
        var subproceso_id = $('input[name=subproceso_id]').val();        
        var procedimiento_id = $(this).attr('id');
        $('#modal-btn-aceptar').html('<a href="' + base_url + 'procedimientos/edit/' + proceso_id + '/' + subproceso_id + '/' + procedimiento_id + '/del" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-doc-procedimiento').modal('show');
    });

    // Eliminación de un control
    $('a.controles').click(function() {
        var id = $(this).attr('id');
        var control_titulo = $('#titulo_' + id).text();
        var params = id.replace(/-/g, '\/');
        $('#modal-eliminar-control-titulo').html(control_titulo);
        $('#modal-btn-aceptar-control').html('<a href="' + base_url + 'controles/delete/' + params + '" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-control').modal('show');
    });

    // Eliminación de un documento de control
    $('a.doc-control').click(function() {
        var control_id = $(this).attr('id');
        var proceso_id = $('input[name=proceso_id]').val();
        var subproceso_id = $('input[name=subproceso_id]').val();
        var procedimiento_id = $('input[name=procedimiento_id]').val();
        $('#modal-btn-aceptar-control').html('<a href="' + base_url + 'controles/edit/' + proceso_id + '/' + subproceso_id + '/' + procedimiento_id + '/' + control_id + '/del" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-doc-control').modal('show');
    });

    // Eliminación de un documento de evidencia
    $('a.doc-evidencia').click(function() {
        var control_id = $(this).attr('id');
        var proceso_id = $('input[name=proceso_id]').val();
        var subproceso_id = $('input[name=subproceso_id]').val();        
        var procedimiento_id = $('input[name=procedimiento_id]').val();
        $('#modal-btn-aceptar-evidencia').html('<a href="' + base_url + 'controles/edit/' + proceso_id + '/' + subproceso_id + '/' + procedimiento_id + '/' + control_id + '/del/evidencia" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-doc-evidencia').modal('show');
    });
    
    // Eliminación de un proceso
    $('a.observaciones').click(function() {
        var observ_id = $(this).attr('id');
        var observacion = $('#observacion_' + observ_id).text();
        $('#modal-eliminar-observaciones-titulo').html(observacion);
        $('#modal-btn-aceptar').html('<a href="' + base_url + 'observaciones/delete/' + observ_id + '" class="btn btn-primary">Aceptar</a>');
        $('#modal-eliminar-observaciones').modal('show');
    });    

}); 