<?php $this->load->view('cumplimiento/procesos/view_navbar') ?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Cumplimiento - Procesos</h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Procesos</th>
                        <th>Fecha de publicación</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php if($procesos): ?>
                        <?php foreach( $procesos as $proceso ): ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url() ?>subprocesos/proceso/<?php echo $proceso->proceso_id ?>" id="titulo_<?php echo $proceso->proceso_id ?>"><?php echo $proceso->proceso_nombre ?></a>
                                </td>
                                <td class="center"><?php echo!empty( $proceso->fecha_mod ) ? '<span class="'. control_style( $proceso->fecha_mod, $this->config->item( 'procesos_plazo_min' ), $this->config->item( 'procesos_plazo_max' ) ) .'">' .fecha_format( $proceso->fecha_mod ) .'</span>' : '<span class="'. control_style( $proceso->fecha_alta, $this->config->item( 'procesos_plazo_min' ), $this->config->item( 'procesos_plazo_max' ) ) .'">' .fecha_format( $proceso->fecha_alta ) .'</span>' ?></td>
                                <td class="center">
                                    <?php if( is_allowed( $controller, 'edit' ) ): ?>
                                        <a class="btn btn-mini btn-info" href="<?php echo base_url() ?>procesos/edit/<?php echo $proceso->proceso_id ?>" id="<?php echo $proceso->proceso_id ?>" title="Modificar"><i class="icon-edit icon-white"></i>  </a>
                                    <?php endif; ?>
                                    <?php if( is_allowed( $controller, 'delete' ) ): ?>
                                        <a class="btn btn-mini btn-danger procesos" href="#" id="<?php echo $proceso->proceso_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i> </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-proceso">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de proceso</h3>
    </div>
    <div class="modal-body">
        <p id="modal-eliminar-proceso-titulo">&nbsp;</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar"></span>
    </div>
</div>