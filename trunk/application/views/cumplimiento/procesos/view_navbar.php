<div>
    <ul class="breadcrumb">
        <?php echo is_allowed( 'procesos', 'index' ) ? '<li><a href="'. base_url( 'procesos' ) .'" >Procesos</a></li><span class="divider">|</span>' : '' ?>
        <?php if( is_allowed( 'procesos', 'add' ) ): ?>
            <li>
                <a href="<?php echo base_url() ?>procesos/add" title="Agregar Proceso">
                    <span title="Agregar Proceso" class="icon icon-color icon-add"></span> Agregar Proceso
                </a> 
            </li>
        <?php endif; ?>
    </ul>
</div>