<?php $this->load->view('cumplimiento/procesos/view_navbar') ?>

<div class="row-fluid sortable">
    <div class="box span12">
        
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Formulario - Procesos</h2>
        </div>
        
        <div class="box-content">
            <?php echo form_open_multipart('procesos/'.$form_action, array('class'=>'form-horizontal')) ?>
                <?php echo form_hidden('form_action', $form_action) ?>
                <fieldset>
                    
                    <div class="control-group">
                        <label class="control-label" for="proceso">Proceso </label>
                        <div class="controls">
                            <?php if($form_action == 'edit'): ?>
                                <?php echo form_hidden('proceso_id', $this->input->post( 'proceso_id' ) ? $this->input->post( 'proceso_id' ) : $proceso->proceso_id) ?>
                                <?php echo form_input( array( 'name' => 'proceso', 'id' => 'proceso', 'class' => 'input-xlarge', 'value' => $this->input->post( 'proceso' ) ? $this->input->post( 'proceso' ) : $proceso->proceso_nombre ) ) ?>
                            <?php else: ?>
                                <?php echo form_input( array( 'name' => 'proceso', 'id' => 'proceso', 'class' => 'input-xlarge', 'value' => set_value( 'proceso' ) ) ) ?>
                            <?php endif; ?>
                            <?php echo form_error( 'proceso' ) ?>
                        </div>
                    </div>

                    <div class="form-actions">
                        <?php echo form_submit(array('name'=>'submit_proceso','class'=>'btn btn-primary','value'=>'Guardar')) ?>
                        <a href="<?php echo base_url() ?>procesos" class="btn">Salir</a>
                    </div>
                    
                </fieldset>
            <?php echo form_close() ?>   

        </div>
    </div><!--/span-->

</div><!--/row-->