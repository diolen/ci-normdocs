<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2>
                <i class="icon-th-list"></i> Cumplimiento - Observaciones
                <?php if( is_allowed( $controller, 'add' ) ): ?>
                    <span class="divider">|</span> <a href="<?php echo base_url( 'observaciones/add' ) ?>" title="Agregar observación"><span title="Agregar observación" class="icon icon-color icon-add"></span> Agregar observación</a>
                <?php endif; ?>            
            </h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>ID <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>Origén <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>Observación <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>Temática <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th title="Estado">E <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th title="Riesgo">R <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>Respuesta <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>Responsable <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>Fecha planificada <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php if( $observaciones ): ?>
                        <?php foreach( $observaciones as $observ ): ?>
                            <tr>
                                <td><?php echo $observ->observ_indentificacion ?></td>
                                <td><?php echo $observ->observ_origen_auditoria .'-'. $observ->observ_origen_ano ?></td>
                                <td id="observacion_<?php echo $observ->observ_id ?>"><?php echo $observ->observ_observacion ?></td>
                                <td><?php echo $observ->observ_tematica ?></td>
                                <td><span class="<?php echo estado_color( $observ->observ_estado )?>" title="<?php echo $estado_ddl[$observ->observ_estado] ?>"><?php echo $observ->observ_estado ?></span></td>
                                <td><span class="<?php echo riesgo_color( $observ->observ_riesgo )?>" title="<?php echo $riesgo_ddl[$observ->observ_riesgo] ?>"><?php echo $observ->observ_riesgo ?></span></td>
                                <td><?php echo $observ->observ_respuesta ?></td>
                                <td><?php echo $observ->observ_responsable ?></td>
                                <td><span class="<?php echo fecha_planificada_color( $observ->observ_fecha_planificada )?>"><?php echo date( "d/m/Y", $observ->observ_fecha_planificada ) ?></span></td>
                                <td>
                                    <?php if( is_allowed( 'observaciones', 'edit' ) ): ?>
                                        <a class="btn btn-mini btn-info" href="<?php echo base_url( 'observaciones/edit/'.$observ->observ_id ) ?>" id="<?php echo $observ->observ_id ?>" title="Modificar conrol"><i class="icon-edit icon-white"></i></a>
                                    <?php endif; ?>                                                                        
                                    <?php if( is_allowed( 'observaciones', 'delete' ) ): ?>
                                        <a class="btn btn-mini btn-danger observaciones" href="#" class="controles" title="Eliminar control" id="<?php echo $observ->observ_id ?>"><i class="icon-trash icon-white"></i></a>
                                    <?php endif; ?>
                                </td>                                
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-observaciones">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de observaciones</h3>
    </div>
    <div class="modal-body">
        <p id="modal-eliminar-observaciones-titulo">&nbsp;</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar"></span>
    </div>
</div>