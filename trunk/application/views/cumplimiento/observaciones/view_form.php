<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Formulario - Observaciones</h2>
        </div>

        <div class="box-content">
            <?php echo form_open( 'observaciones/' . $form_action, array( 'class' => 'form-horizontal' ) ) ?>
            <?php echo form_hidden('form_action', $form_action) ?>
            <?php if( $form_action == 'edit' ): ?>
                <?php echo form_hidden( 'observ_id', $observ->observ_id ) ?>
            <?php endif; ?>
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="observ_indentificacion">ID </label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_input( array( 'name' => 'observ_indentificacion', 'id' => 'observ_indentificacion', 'class' => 'input-xlarge', 'value' => $this->input->post( 'observ_indentificacion' ) ? $this->input->post( 'observ_indentificacion' ) : $observ->observ_indentificacion, 'placeholder' => 'Identificación de la observación' ) ) ?>
                        <?php else: ?>
                            <?php echo form_input( array( 'name' => 'observ_indentificacion', 'id' => 'observ_indentificacion', 'class' => 'input-xlarge', 'value' => set_value( 'observ_indentificacion' ), 'placeholder' => 'Identificación de la observación' ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'observ_indentificacion' ) ?>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="observ_origen">Origén</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_dropdown( 'observ_origen_auditoria', $auditoria_ddl, $this->input->post( 'observ_origen_auditoria' ) ? $this->input->post( 'observ_origen_auditoria' ) : $observ->observ_origen_auditoria, 'class="input input-xlarge"' ) ?>
                            <span class="divider">-</span>
                            <?php echo form_dropdown( 'observ_origen_ano', $ano_ddl, $this->input->post( 'observ_origen_ano' ) ? $this->input->post( 'observ_origen_ano' ) : $observ->observ_origen_ano, 'class="input input-small"' ) ?>
                        <?php else: ?>
                            <?php echo form_dropdown( 'observ_origen_auditoria', $auditoria_ddl, $this->input->post( 'observ_origen_auditoria' ) ? $this->input->post( 'observ_origen_auditoria' ) : NULL, 'class="input input-xlarge"' ) ?>
                            <span class="divider">-</span>                            
                            <?php echo form_dropdown( 'observ_origen_ano', $ano_ddl, $this->input->post( 'observ_origen_ano' ) ? $this->input->post( 'observ_origen_ano' ) : NULL, 'class="input input-small"' ) ?>
                        <?php endif; ?>
                    </div>
                </div>                     

                <div class="control-group">
                    <label class="control-label" for="observ_observacion">Observación</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_textarea( array( 'name' => 'observ_observacion', 'id' => 'observ_observacion', 'rows' => 3, 'class' => 'input-xlarge', 'value' => $this->input->post( 'observ_observacion' ) ? $this->input->post( 'observ_observacion' ) : br2nbsp( $observ->observ_observacion ) ) ) ?>
                        <?php else: ?>
                            <?php echo form_textarea( array( 'name' => 'observ_observacion', 'id' => 'observ_observacion', 'rows' => 3, 'class' => 'input-xlarge', 'value' => set_value( 'observ_observacion' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'observ_observacion' ) ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="observ_tematica">Temática</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_input( array( 'name' => 'observ_tematica', 'id' => 'observ_tematica', 'class' => 'input-xlarge', 'value' => $this->input->post( 'observ_tematica' ) ? $this->input->post( 'observ_tematica' ) : $observ->observ_tematica ) ) ?>
                        <?php else: ?>
                            <?php echo form_input( array( 'name' => 'observ_tematica', 'id' => 'observ_tematica', 'class' => 'input-xlarge', 'value' => set_value( 'observ_tematica' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'observ_tematica' ) ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="observ_estado">Estado</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_dropdown( 'observ_estado', $estado_ddl, $this->input->post( 'observ_origen_ano' ) ? $this->input->post( 'observ_origen_ano' ) : $observ->observ_estado ) ?>
                        <?php else: ?>
                            <?php echo form_dropdown( 'observ_estado', $estado_ddl, $this->input->post( 'observ_origen_ano' ) ? $this->input->post( 'observ_origen_ano' ) : NULL ) ?>
                        <?php endif; ?>
                    </div>
                </div>                

                <div class="control-group">
                    <label class="control-label" for="observ_riesgo">Riesgo</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_dropdown( 'observ_riesgo', $riesgo_ddl, $this->input->post( 'observ_riesgo' ) ? $this->input->post( 'observ_riesgo' ) : $observ->observ_riesgo ) ?>
                        <?php else: ?>
                            <?php echo form_dropdown( 'observ_riesgo', $riesgo_ddl, $this->input->post( 'observ_riesgo' ) ? $this->input->post( 'observ_riesgo' ) : NULL ) ?>
                        <?php endif; ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="observ_respuesta">Respuesta</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_textarea( array( 'name' => 'observ_respuesta', 'id' => 'observ_respuesta', 'rows' => 3, 'class' => 'input-xlarge', 'value' => $this->input->post( 'observ_respuesta' ) ? $this->input->post( 'observ_respuesta' ) : br2nbsp( $observ->observ_respuesta ) ) ) ?>
                        <?php else: ?>
                            <?php echo form_textarea( array( 'name' => 'observ_respuesta', 'id' => 'observ_respuesta', 'rows' => 3, 'class' => 'input-xlarge', 'value' => set_value( 'observ_respuesta' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'observ_respuesta' ) ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="observ_responsable">Responsable</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_textarea( array( 'name' => 'observ_responsable', 'id' => 'observ_responsable', 'rows' => 3, 'class' => 'input-xlarge', 'value' => $this->input->post( 'observ_responsable' ) ? $this->input->post( 'observ_responsable' ) : br2nbsp( $observ->observ_responsable ) ) ) ?>
                        <?php else: ?>
                            <?php echo form_textarea( array( 'name' => 'observ_responsable', 'id' => 'observ_responsable', 'rows' => 3, 'class' => 'input-xlarge', 'value' => set_value( 'observ_responsable' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'observ_responsable' ) ?>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="observ_fecha_planificada">Fecha planificada</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_input( array( 'name' => 'observ_fecha_planificada', 'id' => 'observ_fecha_planificada', 'class' => 'input-xlarge datepicker', 'value' => $this->input->post( 'observ_fecha_planificada' ) ? $this->input->post( 'observ_fecha_planificada' ) : date( "d/m/Y", $observ->observ_fecha_planificada ) ) ) ?>
                        <?php else: ?>
                            <?php echo form_input( array( 'name' => 'observ_fecha_planificada', 'id' => 'observ_fecha_planificada', 'class' => 'input-xlarge datepicker', 'value' => set_value( 'observ_fecha_planificada' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'observ_fecha_planificada' ) ?>                        
                    </div>
                </div>                

                <div class="form-actions">
                    <?php echo form_submit( array( 'name' => 'submit_observ', 'class' => 'btn btn-primary', 'value' => 'Guardar' ) ) ?>
                    <a href="<?php echo base_url( 'observaciones' ) ?>" class="btn">Salir</a>
                </div>

            </fieldset>
            <?php echo form_close() ?>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-doc-control">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $observ->control_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-control"></span>
    </div>
</div>

<div class="modal hide fade" id="modal-eliminar-doc-evidencia">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $observ->control_evidencia_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-evidencia"></span>
    </div>
</div>

<script src="<?php echo base_url() ?>js/cumplimiento/datepicker-es.js"></script>