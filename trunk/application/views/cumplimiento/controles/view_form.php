<?php $this->load->view( 'cumplimiento/view_navbar' ) ?>

<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Formulario - Controles</h2>
        </div>

        <div class="box-content">
            <?php echo form_open_multipart( 'controles/' . $form_action .'/'. $proceso_id .'/'. $subproceso_id .'/'. $procedimiento_id, array( 'class' => 'form-horizontal' ) ) ?>
            <?php echo form_hidden( 'form_action', $form_action ) ?>
            <?php echo form_hidden( 'proceso_id', $proceso_id ) ?>
            <?php echo form_hidden( 'subproceso_id', $subproceso_id ) ?>
            <?php echo form_hidden( 'procedimiento_id', $procedimiento_id ) ?>
            <?php if( $form_action == 'edit' ): ?>
                <?php echo form_hidden( 'control_id', $control_id ) ?>
            <?php endif; ?>
            <fieldset>

                <div class="alert alert-info">
                    <div class="control-group">
                        <label class="control-label" for="control_titulo">Control </label>
                        <div class="controls">
                            <?php if( $form_action == 'edit' ): ?>
                                <?php echo form_input( array( 'name' => 'control_titulo', 'id' => 'control_titulo', 'class' => 'input-xlarge', 'value' => $this->input->post( 'control_titulo' ) ? $this->input->post( 'control_titulo' ) : $control->control_titulo ) ) ?>
                            <?php else: ?>
                                <?php echo form_input( array( 'name' => 'control_titulo', 'id' => 'control_titulo', 'class' => 'input-xlarge', 'value' => set_value( 'control_titulo' ) ) ) ?>
                            <?php endif; ?>
                            <?php echo form_error( 'control_titulo' ) ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="control_doc">Control Doc.</label>
                        <div class="controls">
                            <?php if( $form_action == 'edit' && !empty( $control->control_doc_nombre ) ): ?>
                                <div class="alert alert-success">
                                    Documento adjuntado:<br/>
                                    - <a href="<?php echo base_url() ?>controles/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $procedimiento_id ?>/<?php echo $control->control_id ?>" title="Descargar"><?php echo $control->control_doc_nombre ?></a>
                                    <a class="btn btn-mini btn-danger doc-control" href="#" id="<?php echo $control->control_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i></a>
                                    <p class="help-block">Subir otro documento para reemplazar el actual.</p>                            
                                </div>
                            <?php endif; ?>
                            <?php echo form_upload( array( 'name' => 'control_doc', 'id' => 'control_doc', 'class' => 'input-file uniform_on' ) ) ?>
                        </div>
                    </div>                    
                </div>

                <div class="control-group">
                    <label class="control-label" for="control_ejecucion">Ejecución</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_dropdown( 'control_ejecucion', $ejecucion_ddl, $ejecucion_seleccionada ) ?>
                        <?php else: ?>
                            <?php echo form_dropdown( 'control_ejecucion', $ejecucion_ddl ) ?>
                        <?php endif; ?>
                    </div>
                </div> 

                <div class="control-group">
                    <label class="control-label" for="control_tipo">Tipo</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_dropdown( 'control_tipo', $ejecucion_tipo_ddl, $tipo_seleccionado ) ?>
                        <?php else: ?>
                            <?php echo form_dropdown( 'control_tipo', $ejecucion_tipo_ddl ) ?>
                        <?php endif; ?>
                    </div>
                </div> 

                <div class="control-group">
                    <label class="control-label" for="control_frecuencia">Frecuencia</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_dropdown( 'control_frecuencia', $frecuencia_ddl, $frecuencia_seleccionada ) ?>
                        <?php else: ?>
                            <?php echo form_dropdown( 'control_frecuencia', $frecuencia_ddl ) ?>
                        <?php endif; ?>
                    </div>
                </div>
                
                <div class="alert alert-info">
                    <div class="control-group">
                        <label class="control-label" for="control_evidencia">Evidencia </label>
                        <div class="controls">
                            <?php if( $form_action == 'edit' ): ?>
                                <?php echo form_input( array( 'name' => 'control_evidencia', 'id' => 'control_evidencia', 'class' => 'input-xlarge', 'value' => $this->input->post( 'control_evidencia' ) ? $this->input->post( 'control_evidencia' ) : $control->control_evidencia ) ) ?>
                            <?php else: ?>
                                <?php echo form_input( array( 'name' => 'control_evidencia', 'id' => 'control_evidencia', 'class' => 'input-xlarge', 'value' => set_value( 'control_evidencia' ) ) ) ?>
                            <?php endif; ?>
                            <?php echo form_error( 'control_evidencia' ) ?>
                        </div>
                    </div>                

                    <div class="control-group">
                        <label class="control-label" for="control_evidencia_doc">Evidencia Doc.</label>
                        <div class="controls">
                            <?php if( $form_action == 'edit' && !empty( $control->control_evidencia_doc_nombre ) ): ?>
                                <div class="alert alert-success">
                                    Documento adjuntado:<br/>
                                    - <a href="<?php echo base_url() ?>controles/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $procedimiento_id ?>/<?php echo $control->control_id ?>/evidencia" title="Descargar"><?php echo $control->control_evidencia_doc_nombre ?></a>
                                    <a class="btn btn-mini btn-danger doc-evidencia" href="#" id="<?php echo $control->control_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i></a>
                                    <p class="help-block">Subir otro documento para reemplazar el actual.</p>                            
                                </div>
                            <?php endif; ?>
                            <?php echo form_upload( array( 'name' => 'control_evidencia_doc', 'id' => 'control_evidencia_doc', 'class' => 'input-file uniform_on' ) ) ?>
                        </div>
                    </div>                                    
                </div>

                <div class="form-actions">
                    <?php echo form_submit( array( 'name' => 'submit_control', 'class' => 'btn btn-primary', 'value' => 'Guardar' ) ) ?>
                    <a href="<?php echo base_url() ?>procedimientos/proceso/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>" class="btn">Salir</a>
                </div>

            </fieldset>
            <?php echo form_close() ?>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-doc-control">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $control->control_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-control"></span>
    </div>
</div>

<div class="modal hide fade" id="modal-eliminar-doc-evidencia">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $control->control_evidencia_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-evidencia"></span>
    </div>
</div>