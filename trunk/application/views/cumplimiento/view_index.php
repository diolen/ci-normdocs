<style>
    .cumplimiento-table { font-size: 11px; }
    .cumplimiento-table th,td { border: 1px solid #CCCCCC; }
</style>
<div id="accordion">
    <h3><a href="#">Sección 2. Organizacón Funcional.</a></h3>
    <div>
            <div id="accordion-2-1">
                <h3><a href="#">2.1 Comité de Tecnología Informática. Integración y funciones.</a></h3>
                <div>
                    <div id="accordion-2-1-001">
                        <h3><a href="#">2.1.001</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>
                        <h3><a href="#">2.1.002</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                        <h3><a href="#">2.1.003</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                        <h3><a href="#">2.1.004</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                    </div>
                </div>            
                <h3><a href="#">2.2 Políticas y Procedimiento</a></h3>
                <div>
                    <div id="accordion-2-1-002">
                        <h3><a href="#">2.1.001</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>
                        <h3><a href="#">2.1.002</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                        <h3><a href="#">2.1.003</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                        <h3><a href="#">2.1.004</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                    </div>
                </div>
                <h3><a href="#">2.3 Análisis de Riesgos.</a></h3>
                <div>
                    <div id="accordion-2-1-003">
                        <h3><a href="#">2.1.001</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>
                        <h3><a href="#">2.1.002</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>                        
                        <h3><a href="#">2.1.003</a></h3>
                        <div>
                            <table cellspacing="1" cellpadding="1" width="100%" class="cumplimiento-table">
                                <tr>
                                    <th></th>
                                    <th colspan="3">Cumplimiento</th>
                                    <th></th>
                                    <th></th>
                                </tr>                                
                                <tr>
                                    <th width="30%">Descripción</th>
                                    <th width="15%">No cumple</th>
                                    <th width="15%">Parcialmente</th>
                                    <th width="15%">Cumple</th>
                                    <th width="15%">Plan de acción</th>
                                    <th width="10%">No aplica</th>
                                </tr>
                                <tr>
                                    <td>Existencia del Comité de Tecnología Informática</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Integrado al menos por un miembro del Directorio (o autoridad equivalente) y el responsable máximo del área de Tecnología Informática y Sistemas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Los Integrantes del Comité de Tecnología Informática asumen responsabilidad primaria frente a eventuales incumplimientos a estas normas</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>                            
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <h3><a href="#">Sección 3 Protección de activos de información.</a></h3>
    <div>
        <p>
            Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
            Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
            ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
            lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
        </p>
        <ul>
            <li>List item one</li>
            <li>List item two</li>
            <li>List item three</li>
            <li>List item one</li>
            <li>List item two</li>
            <li>List item three</li>
            <li>List item one</li>
            <li>List item two</li>
            <li>List item three</li>
            <li>List item one</li>
            <li>List item two</li>
            <li>List item three</li>
        </ul>
    </div>
    <h3><a href="#">Sección 4. Continuidad del procesamiento electrónico de datos.</a></h3>
    <div>
        <p>
            Cras dictum. Pellentesque habitant morbi tristique senectus et netus
            et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
            mauris vel est.
        </p>
        <p>
            Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos.
        </p>
    </div>
    <h3><a href="#">Sección 5 Operaciones y procesamiento de datos</a></h3>
    <div>
        <p>
            Cras dictum. Pellentesque habitant morbi tristique senectus et netus
            et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
            mauris vel est.
        </p>
        <p>
            Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos.
        </p>
    </div>
    <h3><a href="#">Sección 6 Banca Electronica (5374).</a></h3>
    <div>
        <p>
            Cras dictum. Pellentesque habitant morbi tristique senectus et netus
            et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
            mauris vel est.
        </p>
        <p>
            Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos.
        </p>
    </div>
    <h3><a href="#">Sección 7. Delegación de Actividades Propias de la Entidad en Terceros.</a></h3>
    <div>
        <p>
            Cras dictum. Pellentesque habitant morbi tristique senectus et netus
            et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
            mauris vel est.
        </p>
        <p>
            Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos.
        </p>
    </div>
    <h3><a href="#">Sección 8.  Sistemas aplicativos de información.</a></h3>
    <div>
        <p>
            Cras dictum. Pellentesque habitant morbi tristique senectus et netus
            et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
            mauris vel est.
        </p>
        <p>
            Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos.
        </p>
    </div>
</div>    