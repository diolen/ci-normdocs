<?php $this->load->view( 'cumplimiento/view_navbar' ) ?>

<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Formulario - Normas</h2>
        </div>

        <div class="box-content">
            <?php echo form_open_multipart( 'normas/' . $form_action .'/'. $proceso_id .'/'. $subproceso_id, array( 'class' => 'form-horizontal' ) ) ?>
            <?php echo form_hidden( 'form_action', $form_action ) ?>
            <?php echo form_hidden( 'proceso_id', $proceso_id ) ?>
            <?php echo form_hidden( 'subproceso_id', $subproceso_id ) ?>
            <?php if( $form_action == 'edit' ): ?>
                <?php echo form_hidden( 'norma_id', $norma_id ) ?>
            <?php endif; ?>
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="norma_titulo">Titulo </label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_input( array( 'name' => 'norma_titulo', 'id' => 'norma_titulo', 'class' => 'input-xlarge', 'value' => $this->input->post( 'norma_titulo' ) ? $this->input->post( 'norma_titulo' ) : $norma->norma_titulo ) ) ?>
                        <?php else: ?>
                            <?php echo form_input( array( 'name' => 'norma_titulo', 'id' => 'norma_titulo', 'class' => 'input-xlarge', 'value' => set_value( 'norma_titulo' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'norma_titulo' ) ?>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="norma_doc">Documento</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' && !empty($norma->norma_doc_nombre) ): ?>
                            <div class="alert alert-info">
                                Documento adjuntado:<br/>
                                - <a href="<?php echo base_url() ?>normas/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $norma->norma_id ?>" title="Descargar"><?php echo $norma->norma_doc_nombre ?></a>
                                <a class="btn btn-mini btn-danger doc-norma" href="#" id="<?php echo $norma->norma_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i></a>
                                <p class="help-block">Subir otro documento para reemplazar el actual.</p>                            
                            </div>
                        <?php endif; ?>
                        <?php echo form_upload( array( 'name' => 'norma_doc', 'id' => 'norma_doc', 'class' => 'input-file uniform_on' ) ) ?>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="norma_requerimientos">Requerimientos</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_textarea( array( 'name' => 'norma_requerimientos', 'id' => 'norma_requerimientos', 'rows' => 3, 'class' => 'input-xlarge', 'value' => $this->input->post( 'norma_requerimientos' ) ? $this->input->post( 'norma_requerimientos' ) : br2nbsp( $norma->norma_req ) ) ) ?>
                        <?php else: ?>
                            <?php echo form_textarea( array( 'name' => 'norma_requerimientos', 'id' => 'norma_requerimientos', 'rows' => 3, 'class' => 'input-xlarge', 'value' => set_value( 'norma_requerimientos' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'norma_requerimientos' ) ?>
                        <p class="help-block">Texto plano sin etiquetas de HTML.</p>
                    </div>
                </div> 

                <div class="form-actions">
                    <?php echo form_submit( array( 'name' => 'submit_norma', 'class' => 'btn btn-primary', 'value' => 'Guardar' ) ) ?>
                    <a href="<?php echo base_url( 'normas/proceso/'.$proceso_id.'/'.$subproceso_id ) ?>" class="btn">Salir</a>
                </div>

            </fieldset>
            <?php echo form_close() ?>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-doc-norma">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $norma->norma_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar"></span>
    </div>
</div>