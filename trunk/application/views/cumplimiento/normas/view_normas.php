<?php $this->load->view('cumplimiento/view_navbar') ?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2>
                <i class="icon-th-list"></i> Subproceso <?php echo $proceso_id ?> - Normas
                <?php if( is_allowed( $controller, 'add' ) ): ?>
                    <span class="divider">|</span> <a href="<?php echo base_url( 'normas/add/'.$proceso_id.'/'.$subproceso_id ) ?>" title="Agregar Normas"><span title="Agregar Normas" class="icon icon-color icon-add"></span> Agregar Normas</a>
                <?php endif; ?>
            </h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Requerimientos</th>
                        <th>Fecha de publicación</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php if($normas): ?>
                        <?php foreach( $normas as $norma ): ?>
                            <tr>
                                <td>
                                    <?php echo form_hidden('subproceso_id_'. $norma->norma_id, $norma->subproceso_id ) ?>
                                    <?php if(!empty($norma->norma_doc_nombre)): ?>
                                        <a href="<?php echo base_url() ?>normas/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $norma->norma_id ?>" id="titulo_<?php echo $norma->norma_id ?>" 
                                           title="Descargar - <?php echo $norma->norma_doc_nombre ?>"><?php echo $norma->norma_titulo ?></a>
                                    <?php else: ?>
                                        <span id="titulo_<?php echo $norma->norma_id ?>"><?php echo $norma->norma_titulo ?></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo $norma->norma_req ?>
                                </td>
                                <td class="center">
                                    <?php echo!empty( $norma->fecha_mod ) ? '<span class="'. control_style( $norma->fecha_mod, $this->config->item( 'normas_plazo_min' ), $this->config->item( 'normas_plazo_max' ) ) .'">' .fecha_format( $norma->fecha_mod ) .'</span>' : '<span class="'. control_style( $norma->fecha_alta, $this->config->item( 'normas_plazo_min' ), $this->config->item( 'normas_plazo_max' ) ) .'">' .fecha_format( $norma->fecha_alta ) .'</span>' ?>
                                </td>
                                <td class="center">
                                    <?php if( is_allowed( $controller, 'edit' ) ): ?>
                                        <a class="btn btn-mini btn-info" href="<?php echo base_url( 'normas/edit/'. $proceso_id.'/'.$norma->subproceso_id .'/'. $norma->norma_id ) ?>"  id="<?php echo $norma->norma_id ?>" title="Modificar"><i class="icon-edit icon-white"></i>  </a>
                                    <?php endif; ?>
                                    <?php if( is_allowed( $controller, 'delete' ) ): ?>
                                        <input type="hidden" name="subproceso_<?php echo $norma->norma_id ?>" id="subproceso_<?php echo $norma->norma_id ?>" value="<?php echo $norma->subproceso_id ?>">
                                        <a class="btn btn-mini btn-danger normas" href="#"  id="<?php echo $norma->norma_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i> </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

<input type="hidden" id="proceso_id" value="<?php echo $proceso_id?>">
<div class="modal hide fade" id="modal-eliminar-norma">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de norma</h3>
    </div>
    <div class="modal-body">
        <p id="modal-eliminar-norma-titulo">&nbsp;</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar"></span>
    </div>
</div>