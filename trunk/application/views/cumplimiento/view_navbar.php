<div>
    <ul class="breadcrumb">
        <?php echo is_allowed( 'procesos', 'index' ) ? '<li><a href="'. base_url( 'procesos' ) .'" title="Procesos">Procesos</a></li>' : '' ?>
        <?php if( $controller == 'usuarios' ): ?>
            <?php echo is_allowed( 'usuarios', 'ad2db' ) ? '<li><span class="divider">|</span> <a href="'. base_url( 'usuarios/ad2db' ) .'" title="Sincronizar DB con Active Directory">Sincronizar DB con Active Directory</a></li>' : '' ?>
        <?php else: ?>
            <?php echo is_allowed( 'subprocesos', 'index' ) ? '<li><span class="divider">|</span> <a href="'. base_url( 'subprocesos/proceso/'. $proceso_id ) .'" title="Subprocesos">Subprocesos</a></li>' : '' ?>
            <?php echo is_allowed( 'normas', 'index' ) ? '<li><span class="divider">|</span> <a href="'. base_url( 'normas/proceso/'. $proceso_id.'/'.$subproceso_id ) .'" title="Normas">Normas</a></li>' : '' ?>
            <?php echo is_allowed( 'procedimientos', 'index' ) ? '<li><span class="divider">|</span> <a href="'. base_url( 'procedimientos/proceso/'. $proceso_id.'/'.$subproceso_id ) .'" title="Procedimientos">Procedimientos</a>' : '' ?>
        <?php endif; ?>
    </ul>
</div>