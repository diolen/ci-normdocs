<?php $this->load->view( 'cumplimiento/view_navbar' ) ?>

<div class="row-fluid sortable">		
    <div class="box span6">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Usuarios - BIND - 4IT</h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
<!--                        <th>usuario</th>-->
                        <th style="cursor:pointer" title="Ordenar por Apellido Nombre">Apellido Nombres <span class="icon icon-color icon-triangle-ns"></span></th>
<!--                        <th>Email</th>-->
                        <th style="cursor:pointer" title="Ordenar por Perfil">Perfil <span class="icon icon-color icon-triangle-ns"></span></th>
                    </tr>
                </thead>   
                <tbody>
                    <?php if( $usuarios ): ?>
                        <?php foreach( $usuarios as $user ): ?>
                            <tr>
        <!--                        <td><?php echo $user->ad_usuario ?></td>-->
                                <td id="nombre_<?php echo $user->ad_usuario ?>"><?php echo $user->ad_displayname ?></td>
        <!--                        <td><?php echo $user->ad_email ?></td>-->
                                <td>
                                    <?php $usuario_perfil = "" ?>
                                    <?php foreach( $perfiles as $perfil ): ?>
                                        <?php if( $user->ad_usuario == $perfil->usuario ): ?>
                                            <?php echo $usuario_perfil = $perfiles_array[ $perfil->perfil ] ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                    <?php if( ( $usuario_perfil != 'desarrollador' ) || $this->usuario->role == 'desarrollador' ): ?>
                                        <?php if( is_allowed( $controller, 'perfil' ) ): ?>
                                            <a class="btn btn-mini btn-info" href="<?php echo base_url() ?>usuarios/perfil/<?php echo $user->ad_usuario ?>"title="Gestión de perfiles"><i class="icon-edit icon-white"></i>  </a>
                                        <?php endif; ?>                                    
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->