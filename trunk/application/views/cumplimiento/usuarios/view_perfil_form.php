<?php $this->load->view( 'cumplimiento/view_navbar' ) ?>

<div class="row-fluid sortable">		
    <div class="box span6">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Usuarios - BIND - 4IT - Perfiles</h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <?php echo form_open('usuarios/perfil/'. $this->uri->segment( 3 ) ) ?>
                <?php echo form_hidden('usuario', $this->uri->segment( 3 ) ) ?>
                <fieldset>
                    
                    <div class="control-group">
                        <label class="control-label" for="proceso">Perfil </label>
                        <div class="controls">
                            <?php echo form_dropdown( 'perfil', usuarios_perfiles_array( $usuario->role ), $perfil ? $perfil->perfil : array() ) ?>
                        </div>
                    </div>

                    <div class="form-actions">
                        <?php echo form_submit( 'submit_deny', 'Bloquear acceso', 'class="btn btn-small btn-danger"' ) ?>
                        <?php echo anchor( 'usuarios', 'Salir', 'class="btn btn-small"' ) ?>
                        <?php echo form_submit( 'submit_perfil', 'Aceptar', 'class="btn btn-small btn-primary"' ) ?>
                    </div>
                    
                </fieldset>            
        </div>
    </div><!--/span-->

</div><!--/row-->