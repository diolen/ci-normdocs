<div class="row-fluid">
    <div class="span12 center login-header">
        <h2>Cumplimiento</h2>
    </div><!--/span-->
</div><!--/row-->

<div class="row-fluid">
    <div class="well span5 center login-box">
        <div class="alert alert-info">
            Inicia sesión con tu nombre de usuario y contraseña.
        </div>
     
        <?php echo form_open('usuarios/login', 'class="form-horizontal"') ?>
            <fieldset>
                <div class="input-prepend" title="Username" data-rel="tooltip">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <?php echo form_input('username', set_value('username'), 'id="username" placeholder="usuario" class="input-large"') ?>
                    <?php echo form_error('username') ?>
                </div>
                <div class="clearfix"></div>

                <div class="input-prepend" title="Password" data-rel="tooltip">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <?php echo form_password('password', set_value('password'), 'id="password" placeholder="contrase&ntilde;a" class="input-large"') ?>
                    <?php echo form_error('password') ?>
                </div>
                <div class="clearfix"></div>

                <p class="center span5">
                    <?php echo form_submit('submit_login', 'Aceptar', 'class="btn btn-primary"') ?>
                </p>
            </fieldset>
        <?php echo form_close() ?>
    </div><!--/span-->
</div><!--/row-->

<script type="text/javascript">
    $(document).ready(function() {
        $('#username').focus();
    });
</script>