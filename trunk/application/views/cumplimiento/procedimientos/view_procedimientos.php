<?php $this->load->view( 'cumplimiento/view_navbar' ) ?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2>
                <i class="icon-th-list"></i> Subproceso <?php echo $this->subproceso_id ?> - Procedimientos
                <?php if( is_allowed( $controller, 'add' ) ): ?>
                    <span class="divider">|</span> <a href="<?php echo base_url() ?>procedimientos/add/<?php echo $this->proceso_id ?>/<?php echo $this->subproceso_id ?>" title="Agregar Procedimientos"><span title="Agregar Procedimientos" class="icon icon-color icon-add"></span> Agregar Procedimientos</a>
                <?php endif; ?>                
            </h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="15%">Titulo <span class="icon icon-color icon-triangle-ns"></span></th>
                        <th width="10%">Requerimientos</th>
                        <th width="70%">
                            <table class="table-controles">
                                <thead>
                                    <tr>
                                        <td width="35%" style="border-left:0">Controles</th>
                                        <td width="6%" title="Ejecución">E</td>
                                        <td width="6%" title="Tipo">T</td>
                                        <td width="6%" title="Frecuencia">F</td>
                                        <td width="35%">Evidencia</td>
                                        <td width="6%">Fecha</td>
                                        <td width="6%" style="border-left:0;"></td>
                                    </tr>
                                </thead>
                            </table>
                        </th>
                        <th width="5%">&nbsp;</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php if( $procedimientos ): ?>
                        <?php foreach( $procedimientos as $proc ): ?>
                            <tr>
                                <td>
                                    <?php echo form_hidden( 'subproceso_id_' . $proc->procedimiento_id, $proc->subproceso_id ) ?>
                                    <?php if( !empty( $proc->procedimiento_doc_nombre ) ): ?>
                                        <a href="<?php echo base_url() ?>procedimientos/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $proc->procedimiento_id ?>" id="titulo_<?php echo $proc->procedimiento_id ?>" title="Descargar - <?php echo $proc->procedimiento_doc_nombre ?>"><?php echo $proc->procedimiento_titulo ?></a>
                                    <?php else: ?>
                                        <span id="titulo_<?php echo $proc->procedimiento_id ?>"><?php echo $proc->procedimiento_titulo ?></span>
                                    <?php endif; ?>
                                </td>
                                <td><?php echo $proc->procedimiento_req ?></td>


                                <td>


                                    <table class="table-controles-rows" style="border: 1px solid #dddddd;">
                                        <tbody>
                                            <?php if( $controles ): ?>
                                                <?php foreach( $controles as $cont ): ?>
                                                    <?php if( $cont->procedimiento_id == $proc->procedimiento_id ): ?>
                                                        <tr>
                                                            <td width="35%" id="titulo_<?php echo $proceso_id . '-' . $subproceso_id . '-' . $proc->procedimiento_id . '-' . $cont->control_id ?>">
                                                                <?php if( !empty( $cont->control_doc_nombre ) ): ?>
                                                                    <a href="<?php echo base_url() ?>controles/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $proc->procedimiento_id ?>/<?php echo $cont->control_id ?>" id="titulo_<?php echo $cont->control_id ?>" title="Descargar - <?php echo $cont->control_doc_nombre ?>"><?php echo $cont->control_titulo ?></a>
                                                                <?php else: ?>                                                                
                                                                    <?php echo $cont->control_titulo ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td width="6%"><span title="<?php echo $ejecucion_ddl[ $cont->control_ejecucion ] ?>" class="<?php echo ejecucion_color( $cont->control_ejecucion ) ?>"><?php echo $cont->control_ejecucion ?></span></td>
                                                            <td width="6%"><span title="<?php echo $ejecucion_tipo_ddl[ $cont->control_tipo ] ?>" class="<?php echo tipo_color( $cont->control_tipo ) ?>"><?php echo $cont->control_tipo ?></span></td>
                                                            <td width="6%"><span title="<?php echo $frecuencia_ddl[ $cont->control_frecuencia ] ?>" class="<?php echo frecuencia_color( $cont->control_frecuencia ) ?>"><?php echo $cont->control_frecuencia ?></span></td>
                                                            <td width="35%">
                                                                <?php if( !empty( $cont->control_evidencia_doc_nombre ) ): ?>
                                                                    <a href="<?php echo base_url() ?>controles/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $proc->procedimiento_id ?>/<?php echo $cont->control_id ?>/evidencia" id="titulo_<?php echo $cont->control_id ?>" title="Descargar - <?php echo $cont->control_evidencia_doc_nombre ?>"><?php echo $cont->control_evidencia ?></a>
                                                                <?php else: ?>                                                                
                                                                    <?php echo $cont->control_evidencia ?>
                                                                <?php endif; ?>                                                                
                                                            </td>
                                                            <td width="6%"><?php echo!empty( $cont->fecha_mod ) ? '<span class="'. control_style( $cont->fecha_mod, $this->config->item( 'procedimientos_plazo_min' ), $this->config->item( 'procedimientos_plazo_max' ) ) .'">' .fecha_format( $cont->fecha_mod ) .'</span>' : '<span class="'. control_style( $cont->fecha_alta, $this->config->item( 'procedimientos_plazo_min' ), $this->config->item( 'procedimientos_plazo_max' ) ) .'">' .fecha_format( $cont->fecha_alta ) .'</span>' ?></td>
                                                            <td width="6%" style="padding:4px 0px 0px 2px;">
                                                                <div class="btn-group">
                                                                    <a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#" title="Administrar controles"><i class="icon icon-color icon-wrench"></i></a>
                                                                    <ul class="dropdown-menu">
                                                                        <?php if( is_allowed( 'controles', 'edit' ) ): ?>
                                                                            <li><a href="<?php echo base_url() ?>controles/edit/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $proc->procedimiento_id ?>/<?php echo $cont->control_id ?>" id="<?php echo $proc->procedimiento_id ?>" title="Modificar conrol"><i class="icon-pencil"></i> Modificar Control</a></li>
                                                                        <?php endif; ?>                                                                        
                                                                        <?php if( is_allowed( 'controles', 'delete' ) ): ?>
                                                                            <li><a href="#" class="controles" title="Eliminar control" id="<?php echo $proceso_id . '-' . $subproceso_id . '-' . $proc->procedimiento_id . '-' . $cont->control_id ?>"><i class="icon-trash"></i> Eliminar Control</a></li>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>                                                    
                                                            </td>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>



                                </td>
                                <td class="center">
                                    <div class="btn-group">
                                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" title="Administrar procedimientos"><i class="icon icon-white icon-gear"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php if( is_allowed( $controller, 'edit' ) ): ?>
                                                <li><a href="<?php echo base_url() ?>procedimientos/edit/<?php echo $proceso_id ?>/<?php echo $proc->subproceso_id ?>/<?php echo $proc->procedimiento_id ?>" id="<?php echo $proc->procedimiento_id ?>" title="Modificar Procedimiento"><i class="icon-pencil"></i> Modificar Procedimiento</a></li>
                                            <?php endif; ?>
                                            <?php if( is_allowed( $controller, 'delete' ) ): ?>
                                                <li><a href="#" class="procedimientos" title="Eliminar Procedimiento" id="<?php echo $proc->procedimiento_id ?>"><i class="icon-trash"></i> Eliminar Procedimiento</a></li>
                                            <?php endif; ?>                                            
                                            <?php if( is_allowed( 'controles', 'add' ) ): ?>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo base_url() ?>controles/add/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $proc->procedimiento_id ?>" title="Agregar controles"><i class="icon-plus"></i> Agregar Controles</a></li>
                                            <?php endif; ?>                                            
                                        </ul>
                                    </div>                                    
                                </td>                                
                            </tr>  
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->
<input type="hidden" name="proceso_id" value="<?php echo $proceso_id ?>">
<input type="hidden" name="subproceso_id" value="<?php echo $subproceso_id ?>">

<div class="modal hide fade" id="modal-eliminar-procedimiento">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de procedimiento</h3>
    </div>
    <div class="modal-body">
        <p id="modal-eliminar-procedimiento-titulo">&nbsp;</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-procedimiento"></span>
    </div>
</div>



<div class="modal hide fade" id="modal-eliminar-control">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de control</h3>
    </div>
    <div class="modal-body">
        <p id="modal-eliminar-control-titulo">&nbsp;</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-control"></span>
    </div>
</div>