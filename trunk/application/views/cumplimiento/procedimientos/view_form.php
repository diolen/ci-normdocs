<?php $this->load->view( 'cumplimiento/view_navbar' ) ?>

<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Formulario - Normas</h2>
        </div>

        <div class="box-content">
            <?php echo form_open_multipart( 'procedimientos/' . $form_action .'/'. $subproceso_id, array( 'class' => 'form-horizontal' ) ) ?>
            <?php echo form_hidden( 'form_action', $form_action ) ?>
            <?php echo form_hidden( 'proceso_id', $proceso_id ) ?>
            <?php echo form_hidden( 'subproceso_id', $subproceso_id ) ?>
            <?php if( $form_action == 'edit' ): ?>
                <?php echo form_hidden( 'procedimiento_id', $procedimiento_id ) ?>
            <?php endif; ?>
            <fieldset>

                <div class="alert alert-info">
                    <div class="control-group">
                        <label class="control-label" for="procedimiento_titulo">Titulo </label>
                        <div class="controls">
                            <?php if( $form_action == 'edit' ): ?>
                                <?php echo form_input( array( 'name' => 'procedimiento_titulo', 'id' => 'procedimiento_titulo', 'class' => 'input-xlarge', 'value' => $this->input->post( 'procedimiento_titulo' ) ? $this->input->post( 'procedimiento_titulo' ) : $procedimiento->procedimiento_titulo ) ) ?>
                            <?php else: ?>
                                <?php echo form_input( array( 'name' => 'procedimiento_titulo', 'id' => 'procedimiento_titulo', 'class' => 'input-xlarge', 'value' => set_value( 'procedimiento_titulo' ) ) ) ?>
                            <?php endif; ?>
                            <?php echo form_error( 'procedimiento_titulo' ) ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="procedimiento_doc">Titulo Doc.</label>
                        <div class="controls">
                            <?php if( $form_action == 'edit' && !empty( $procedimiento->procedimiento_doc_nombre ) ): ?>
                                <div class="alert alert-success">
                                    Documento adjuntado:<br/>
                                    - <a href="<?php echo base_url() ?>procedimientos/descargar/<?php echo $proceso_id ?>/<?php echo $subproceso_id ?>/<?php echo $procedimiento->procedimiento_id ?>" title="Descargar"><?php echo $procedimiento->procedimiento_doc_nombre ?></a>
                                    <a class="btn btn-mini btn-danger doc-procedimiento" href="#" id="<?php echo $procedimiento->procedimiento_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i></a>
                                    <p class="help-block">Subir otro documento para reemplazar el actual.</p>                            
                                </div>
                            <?php endif; ?>
                            <?php echo form_upload( array( 'name' => 'procedimiento_doc', 'id' => 'procedimiento_doc', 'class' => 'input-file uniform_on' ) ) ?>
                        </div>
                    </div>                    
                </div>

                <div class="control-group">
                    <label class="control-label" for="procedimiento_requerimientos">Requerimientos</label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_textarea( array( 'name' => 'procedimiento_requerimientos', 'id' => 'procedimiento_requerimientos', 'rows' => 3, 'class' => 'input-xlarge', 'value' => $this->input->post( 'procedimiento_requerimientos' ) ? $this->input->post( 'procedimiento_requerimientos' ) : br2nbsp( $procedimiento->procedimiento_req ) ) ) ?>
                        <?php else: ?>
                            <?php echo form_textarea( array( 'name' => 'procedimiento_requerimientos', 'id' => 'procedimiento_requerimientos', 'rows' => 3, 'class' => 'input-xlarge', 'value' => set_value( 'procedimiento_requerimientos' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'procedimiento_requerimientos' ) ?>
                    </div>
                </div> 

                <div class="form-actions">
                    <?php echo form_submit( array( 'name' => 'submit_procedimiento', 'class' => 'btn btn-primary', 'value' => 'Guardar' ) ) ?>
                    <a href="<?php echo base_url() ?>procedimientos/proceso/<?php echo $subproceso_id ?>" class="btn">Salir</a>
                </div>

            </fieldset>
            <?php echo form_close() ?>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-doc-procedimiento">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $procedimiento->procedimiento_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar"></span>
    </div>
</div>

<div class="modal hide fade" id="modal-eliminar-doc-evidencia">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de documento</h3>
    </div>
    <div class="modal-body">
        <p><strong>Documento a eliminar:</strong> <?php echo $procedimiento->procedimiento_evidencia_doc_nombre ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar-evidencia"></span>
    </div>
</div>