<?php $this->load->view('cumplimiento/subprocesos/view_navbar') ?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Cumplimiento - Subprocesos</h2>
            <div class="box-icon"></div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Subprocesos</th>
                        <th>Fecha de publicación</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php if($subprocesos): ?>
                        <?php foreach( $subprocesos as $subproceso ): ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url( 'normas/proceso/'.$proceso_id .'/'.$subproceso->subproceso_id ) ?>" id="titulo_<?php echo $subproceso->subproceso_id ?>"><?php echo $subproceso->subproceso_nombre ?></a>
                                </td>
                                <td class="center">
                                    <?php echo!empty( $subproceso->fecha_mod ) ? '<span class="'. control_style( $subproceso->fecha_mod, $this->config->item( 'subprocesos_plazo_min' ), $this->config->item( 'subprocesos_plazo_max' ) ) .'">' .fecha_format( $subproceso->fecha_mod ) .'</span>' : '<span class="'. control_style( $subproceso->fecha_alta, $this->config->item( 'subprocesos_plazo_min' ), $this->config->item( 'subprocesos_plazo_max' ) ) .'">' .fecha_format( $subproceso->fecha_alta ) .'</span>' ?>
                                </td>
                                <td class="center">
                                    <?php if( is_allowed( $controller, 'edit' ) ): ?>
                                        <a class="btn btn-mini btn-info" href="<?php echo base_url( 'subprocesos/edit/'. $subproceso->proceso_id .'/'. $subproceso->subproceso_id ) ?>" id="<?php echo $subproceso->subproceso_id ?>" title="Modificar"><i class="icon-edit icon-white"></i>  </a>
                                    <?php endif; ?>
                                    <?php if( is_allowed( $controller, 'delete' ) ): ?>
                                        <input type="hidden" name="proceso_id" id="proceso_<?php echo $subproceso->subproceso_id ?>" value="<?php echo $subproceso->proceso_id ?>">
                                        <a class="btn btn-mini btn-danger subprocesos" href="#" id="<?php echo $subproceso->subproceso_id ?>" title="Eliminar"><i class="icon-trash icon-white"></i> </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="modal hide fade" id="modal-eliminar-subproceso">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Eliminación de subproceso</h3>
    </div>
    <div class="modal-body">
        <p id="modal-eliminar-subproceso-titulo">&nbsp;</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Salir</a>
        <span id="modal-btn-aceptar"></span>
    </div>
</div>