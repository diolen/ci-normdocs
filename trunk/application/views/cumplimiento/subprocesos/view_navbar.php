<div>
    <ul class="breadcrumb">
        <?php echo is_allowed( 'procesos', 'index' ) ? '<li><a href="'. base_url( 'procesos' ) .'" >Procesos</a></li><span class="divider">|</span>' : '' ?>
        <?php echo is_allowed( 'subprocesos', 'index' ) ? '<li><a href="'. base_url( 'subprocesos/proceso/'. $proceso_id ) .'" >Subrocesos</a></li><span class="divider">|</span>' : '' ?>
        <?php if( is_allowed( 'subprocesos', 'add' ) ): ?>
            <li>
                <a href="<?php echo base_url( 'subprocesos/add/'. $proceso_id ) ?>" title="Agregar Subproceso">
                    <span title="Agregar Subproceso" class="icon icon-color icon-add"></span> Agregar subproceso
                </a> 
            </li>
        <?php endif; ?>
    </ul>
</div>