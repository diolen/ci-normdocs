<?php $this->load->view( 'cumplimiento/subprocesos/view_navbar' ) ?>

<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Formulario - Subprocesos</h2>
        </div>

        <div class="box-content">
            <?php echo form_open_multipart( 'subprocesos/' . $form_action . '/' . $proceso_id, array( 'class' => 'form-horizontal' ) ) ?>
            <?php echo form_hidden( 'form_action', $form_action ) ?>
            <?php echo form_hidden( 'proceso_id', $proceso_id ) ?>
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="subproceso">Subproceso </label>
                    <div class="controls">
                        <?php if( $form_action == 'edit' ): ?>
                            <?php echo form_hidden( 'subproceso_id', $subproceso_id ? $subproceso_id : $subproceso->subproceso_id ) ?>
                            <?php echo form_input( array( 'name' => 'subproceso', 'id' => 'subproceso', 'class' => 'input-xlarge', 'value' => $this->input->post( 'subproceso' ) ? $this->input->post( 'subproceso' ) : $subproceso->subproceso_nombre ) ) ?>
                        <?php else: ?>
                            <?php echo form_input( array( 'name' => 'subproceso', 'id' => 'subproceso', 'class' => 'input-xlarge', 'value' => set_value( 'subproceso' ) ) ) ?>
                        <?php endif; ?>
                        <?php echo form_error( 'subproceso' ) ?>
                    </div>
                </div>

                <div class="form-actions">
                    <?php echo form_submit( array( 'name' => 'submit_subproceso', 'class' => 'btn btn-primary', 'value' => 'Guardar' ) ) ?>
                    <a href="<?php echo base_url( 'subprocesos/proceso/'. $this->uri->segment( 3 ) ) ?>" class="btn">Salir</a>
                </div>

            </fieldset>
            <?php echo form_close() ?>   

        </div>
    </div><!--/span-->

</div><!--/row-->