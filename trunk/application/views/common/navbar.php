<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <div id="HeaderNew">
                <a href="<?php echo base_url() ?>"><?php echo img('img/cumplimiento/logo-bind-nuevo.png') ?></a>
            </div>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
<!--                    <li><a href="<?php echo base_url() ?>" >Inicio</a></li>-->
                    <?php echo is_allowed( 'procesos', 'index' ) ? '<li><a href="'. base_url( 'procesos' ) .'" >Procesos</a></li>' : '' ?>
                    <?php echo is_allowed( 'observaciones', 'index' ) ? '<li><a href="'. base_url( 'observaciones' ) .'" >Observaciones</a></li>' : '' ?>
                    <?php echo is_allowed( 'usuarios', 'index' ) ? '<li><a href="'. base_url( 'usuarios' ) .'" >Usuarios</a></li>' : '' ?>
                </ul>
                
                <ul class="nav pull-right">
                    <li class="dropdown margin-menu">
                        <!-- user dropdown starts -->
                        <div class="btn-group pull-right" >
                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="icon-user"></i><span class="hidden-phone"><?php echo $usuario->username ?></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url() . 'usuarios/logout'; ?>">Salir</a></li>
                            </ul>
                        </div>
                        <!-- user dropdown ends -->
                    </li>
                </ul>

            </div><!-- /.nav-collapse -->
        </div>
    </div><!-- /navbar-inner -->
</div> 