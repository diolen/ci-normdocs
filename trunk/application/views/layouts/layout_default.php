<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('common/header') ?>
    </head>

    <body>
        
	<?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
            <!-- topbar starts -->
            <?php $this->load->view('common/navbar') ?>
            <!-- topbar ends -->
        <?php } ?>
        
        <div class="container-fluid">
            <div class="row-fluid">
                <?php if(!isset($no_visible_elements) || !$no_visible_elements) { ?>                    
                    <div id="content" class="span12">
                        <!-- content starts -->
                <?php } ?>  
                        
                        <?php echo $this->session->flashdata('flashdata') ?>
                        <?php echo $content ?>
            
                        <?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
                        <!-- content ends -->
                    </div><!--/#content.span10-->
                        <?php } ?>
                    
            </div><!--/fluid-row-->
            
            <?php if(!isset($no_visible_elements) || !$no_visible_elements) { ?>            
                <hr>
                <?php $this->load->view('charisma/modal') ?>
                <?php $this->load->view('common/footer') ?>
            <?php } ?>

        </div><!--/.fluid-container-->

    </body>
</html>
