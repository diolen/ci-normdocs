<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Observaciones extends BIND_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->is_logged();
        
        if( !is_allowed( $this->controller, $this->action ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }
        
        $this->load->model('observaciones_model');
        $this->layout->set( 'auditoria_ddl', $this->observaciones_model->auditoria_ddl() );
        $this->layout->set( 'ano_ddl', $this->observaciones_model->ano_ddl() );
        $this->layout->set( 'estado_ddl', $this->observaciones_model->estado_ddl() );
        $this->layout->set( 'riesgo_ddl', $this->observaciones_model->riesgo_ddl() );
    }
    
    public function index()
    {
        $this->layout->set( 'observaciones', $this->observaciones_model->get_observaciones() );        
        $this->layout->view('cumplimiento/observaciones/view_observaciones');
    }
    
    public function add()
    {
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'add' );
        if( $this->input->post( 'submit_observ' ) ) {
            $this->observaciones_model->valid_form();
            if( $this->form_validation->run() != FALSE ) {
                if( $this->observaciones_model->save() !== FALSE ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_save' ) );
                }
                redirect( 'observaciones' );
            }
        }
        $this->layout->view( 'cumplimiento/observaciones/view_form' );
    }

    public function edit()
    {
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'edit' );
        $this->layout->set( 'observ', $this->observaciones_model->get_observaciones() );

        if( $this->input->post( 'submit_observ' ) ) {
            $this->observaciones_model->valid_form();
            if( $this->form_validation->run() != FALSE ) {
                if( $this->observaciones_model->save() !== FALSE ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_save' ) );
                }
                redirect( 'observaciones' );
            }
        }
        $this->layout->view( 'cumplimiento/observaciones/view_form' );
    }

    public function delete()
    {
        if( $this->observaciones_model->delete() ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_delete' ) );
        } else {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
        }
        redirect( 'observaciones' );
    }    

}