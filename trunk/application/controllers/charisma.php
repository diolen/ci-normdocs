<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Charisma extends CI_Controller {

    public function index()
    {
        $tpl = $this->uri->segment(2) ? $this->uri->segment(2) : 'index';
        $this->layout->view('charisma/' . $tpl);
    }

    public function tpl()
    {
        $tpl = $this->uri->segment(2) ? $this->uri->segment(2) : 'index';
        $this->layout->view('charisma/' . $tpl);
    }

}