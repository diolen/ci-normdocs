<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends BIND_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->layout->set('perfil_usuario', '');
    }

    public function index()
    {
        if( !is_allowed( $this->controller, 'index' ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }
        $this->is_logged();
        $this->layout->set( 'perfiles', $this->usuarios_model->get_perfiles() );
        $this->layout->set( 'perfiles_array', usuarios_perfiles_array( 'desarrollador' ) );
        $this->layout->set( 'usuarios', $this->usuarios_model->get_users() );
        $this->layout->view( 'cumplimiento/usuarios/view_usuarios' );
    }

    public function login()
    {
        $this->layout->set_title( 'Cumplimientos - Login' );
        $this->layout->set( 'mostrar_header', TRUE );
        $this->layout->set( 'no_visible_elements', TRUE );
        $this->load->library( 'form_validation' );

        if( $this->input->post( 'submit_login' ) ) {

            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'username', 'Usuario', 'trim|required|min_length[5]|max_length[12]|xss_clean' );
            $this->form_validation->set_rules( 'password', 'Contrase&ntilde;a', 'trim|required|xss_clean' );

            if( $this->form_validation->run() != FALSE ) {
                try {
                    $this->usuarios_model->login( $this->input->post( 'username' ), $this->input->post( 'username' ), $this->input->post( 'password' ) );
                    redirect( base_url() );
                } catch( Exception $e ) {
                    $this->session->set_flashdata( 'flashdata', '<div class="alert alert-error">Usuario o Contrase&ntilde;a incorrecto.</div>' );
                    redirect( 'usuarios/login' );
                }
            }
        }
        $this->layout->view( 'cumplimiento/usuarios/view_login' );
    }

    public function logout()
    {
        //$this->session->sess_destroy();
        Zend_Session::destroy();
        redirect( 'usuarios/login' );
    }

    public function ad2db()
    {
        if( !is_allowed( $this->controller, 'ad2db' ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }
        $this->is_logged();
        if( $this->usuarios_model->db_sincronizar() ) {
            redirect( 'usuarios' );
        } else {
            redirect( base_url() );
        }
    }

    public function perfil()
    {
        if( !is_allowed( $this->controller, 'perfil' ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }
        $this->is_logged();
        $this->layout->set( 'perfil', $this->usuarios_model->get_perfiles($this->uri->segment(3)) );
        $this->layout->set( 'perfiles', usuarios_perfiles_array( $this->usuario->role ) );
        
        if( $this->input->post( 'submit_perfil' ) ) {
            if( $this->usuarios_model->save_perfil() !== FALSE ) {
                $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                redirect( 'usuarios' );
            } else {
                $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_save' ) );
                redirect( 'usuarios' );
            }
        }
        
        if( $this->input->post( 'submit_deny' ) ) {
            if( $this->usuarios_model->delete_perfil() !== FALSE ) {
                $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                redirect( 'usuarios' );
            } else {
                $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
                redirect( 'usuarios' );
            }
        }
        $this->layout->view( 'cumplimiento/usuarios/view_perfil_form' );
    }

}
