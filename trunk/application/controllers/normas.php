<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Normas extends BIND_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->is_logged();

        if( !is_allowed( $this->controller, $this->action ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }

        $this->norma_id = $this->normas_model->get_norma_id();
        $this->layout->set( 'norma_id', $this->norma_id );
    }

    public function index()
    {
        $this->layout->set( 'normas', $this->normas_model->get_normas() );
        $this->layout->view( 'cumplimiento/normas/view_normas' );
    }

    public function add()
    {
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'add' );
        if( $this->input->post( 'submit_norma' ) ) {
            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'norma_titulo', 'Titulo', 'trim|required|min_length[5]|max_length[255]|xss_clean' );
            $this->form_validation->set_rules( 'norma_requerimientos', 'Requerimientos', 'trim|required|min_length[5]|xss_clean' );

            if( $this->form_validation->run() != FALSE ) {
                if( $this->normas_model->save() !== FALSE ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'normas/proceso/' . $this->proceso_id . '/' . $this->subproceso_id );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->normas_model->get_error_message( 'add' ) );
                    redirect( 'normas/proceso/' . $this->proceso_id . '/' . $this->subproceso_id );
                }
            }
        }
        $this->layout->view( 'cumplimiento/normas/view_form' );
    }

    public function edit()
    {
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'edit' );
        $this->layout->set( 'norma', $this->normas_model->get_normas() );

        if( $this->uri->segment( 6 ) == 'del' ) {
            if( $this->normas_model->delete_document() === FALSE ) {
                $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete_img' ) );
            }
            redirect( 'normas/edit/' . $this->proceso_id . '/' . $this->subproceso_id . '/' . $this->norma_id );
        }
        if( $this->input->post( 'submit_norma' ) ) {
            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'norma_titulo', 'Titulo', 'trim|required|min_length[5]|max_length[255]|xss_clean' );
            $this->form_validation->set_rules( 'norma_requerimientos', 'Requerimientos', 'trim|required|min_length[5]|xss_clean' );

            if( $this->form_validation->run() != FALSE ) {
                if( $this->normas_model->save() !== FALSE ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'normas/proceso/' . $this->proceso_id . '/' . $this->subproceso_id );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->normas_model->get_error_message( 'add' ) );
                    redirect( 'normas/proceso/' . $this->proceso_id . '/' . $this->subproceso_id );
                }
            }
        }
        $this->layout->view( 'cumplimiento/normas/view_form' );
    }

    public function delete()
    {
        if( $this->normas_model->delete() ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_delete' ) );
        } else {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
        }
        redirect( 'normas/proceso/' . $this->proceso_id . '/' . $this->subproceso_id );
    }

    public function descargar()
    {
        $this->normas_model->get_document();
    }

}
