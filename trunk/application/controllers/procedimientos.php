<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procedimientos extends BIND_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->is_logged();
        
        if( !is_allowed( $this->controller, $this->action ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }
        
        $this->procedimiento_id = $this->procedimientos_model->get_procedimiento_id();
        $this->layout->set( 'procedimiento_id', $this->procedimiento_id );
        $this->layout->set( 'ejecucion_ddl', $this->controles_model->get_ejecucion_ddl() );
        $this->layout->set( 'ejecucion_tipo_ddl', $this->controles_model->get_ejecucion_tipo_ddl() );
        $this->layout->set( 'frecuencia_ddl', $this->controles_model->get_frecuencia_ddl() );
        
    }

    public function index()
    {
        $this->layout->set( 'procedimientos', $this->procedimientos_model->get_procedimientos() );
        $this->layout->set( 'controles', $this->controles_model->get_controles() );
        $this->layout->view( 'cumplimiento/procedimientos/view_procedimientos' );
    }

    public function add()
    {
        $this->load->library( 'form_validation' );
        
        $form_action = 'add';
        $this->layout->set( 'form_action', $form_action );

        if( $this->input->post( 'submit_procedimiento' ) ) {
            $this->procedimientos_model->valid_form();
            if( $this->form_validation->run() != FALSE ) {
                if($this->procedimientos_model->save() !== FALSE) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'procedimientos/proceso/' . $this->proceso_id.'/'.$this->subproceso_id );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->procedimientos_model->get_error_message( $form_action ) );
                    redirect( 'procedimientos/add/' . $this->proceso_id.'/'.$this->subproceso_id );
                }
            }
        }
        $this->layout->view( 'cumplimiento/procedimientos/view_form' );
    }
    
    public function edit()
    {
        $this->load->library( 'form_validation' );
        $form_action = 'edit';
        $this->layout->set( 'form_action', $form_action );
        $this->layout->set( 'procedimiento', $this->procedimientos_model->get_procedimientos() );
        if( strpos( uri_string(), 'del' ) !== FALSE ) {
            if( $this->procedimientos_model->delete_document() === FALSE ) {
                $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete_img' ) );
            }
            redirect( 'procedimientos/edit/' . $this->proceso_id .'/'. $this->subproceso_id . '/' . $this->procedimiento_id );
        }
        if( $this->input->post( 'submit_procedimiento' ) ) {
            $this->procedimientos_model->valid_form();
            if( $this->form_validation->run() != FALSE ) {
                if( $this->procedimientos_model->save() !== FALSE ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'procedimientos/proceso/' . $this->proceso_id .'/'. $this->subproceso_id );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->procedimientos_model->get_error_message( 'add' ) );
                    redirect( 'procedimientos/add/' . $this->proceso_id .'/'. $this->subproceso_id );
                }
            }
        }
        $this->layout->view( 'cumplimiento/procedimientos/view_form' );
    }

    public function delete()
    {
        if($this->procedimientos_model->delete()) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_delete' ) );       
        } else {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
        }        
        redirect( 'procedimientos/proceso/'.$this->proceso_id.'/'.$this->subproceso_id );
    }
    
    public function descargar()
    {
        $this->procedimientos_model->get_document();
    }

}
