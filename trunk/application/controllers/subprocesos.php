<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subprocesos extends BIND_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->is_logged();

        if( !is_allowed( $this->controller, $this->action ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }
    }

    public function index()
    {
        $this->layout->set( 'subprocesos', $this->subprocesos_model->get_subprocesos() );
        $this->layout->view( 'cumplimiento/subprocesos/view_subprocesos' );
    }

    public function add()
    {
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'add' );
        if( $this->input->post( 'submit_subproceso' ) ) {
            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'subproceso', 'Subproceso', 'trim|required|min_length[5]|max_length[255]|xss_clean' );

            if( $this->form_validation->run() != FALSE ) {
                if( $this->subprocesos_model->save() !== FALSE ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'subprocesos/proceso/' . $this->proceso_id );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
                    redirect( 'subprocesos/' );
                }
            }
        }
        $this->layout->view( 'cumplimiento/subprocesos/view_form' );
    }

    public function edit()
    {
        $this->layout->set( 'subproceso', $this->subprocesos_model->get_subprocesos() );
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'edit' );
        if( $this->input->post( 'submit_subproceso' ) ) {
            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'subproceso', 'Subproceso', 'trim|required|min_length[5]|max_length[255]|xss_clean' );

            if( $this->form_validation->run() != FALSE ) {
                if( $this->subprocesos_model->save() ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'subprocesos/proceso/' . $this->proceso_id );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
                    redirect( 'subprocesos/proceso/' . $this->proceso_id );
                }
            }
        }
        $this->layout->view( 'cumplimiento/subprocesos/view_form' );
    }

    public function delete()
    {
        if( $this->subprocesos_model->delete() ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_delete' ) );
        } else {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
        }
        redirect( 'subprocesos/proceso/' . $this->proceso_id );
    }

}
