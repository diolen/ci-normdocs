<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procesos extends BIND_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->is_logged();
        
        if( !is_allowed( $this->controller, $this->action ) ) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_permisos' ) );
            redirect( base_url() );
        }                
    }
    
    public function index()
    {
        $this->layout->set('procesos', $this->procesos_model->get_procesos());
        $this->layout->view( 'cumplimiento/procesos/view_procesos' );
    }
    
    public function add()
    {
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'add' );
        if( $this->input->post( 'submit_proceso' ) ) {
            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'proceso', 'Proceso', 'trim|required|min_length[5]|max_length[255]|xss_clean' );
            
            if( $this->form_validation->run() != FALSE ) {
                if($this->procesos_model->save() !== FALSE) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'procesos/' );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
                    redirect( 'procesos/' );
                }
            }
        }
        $this->layout->view( 'cumplimiento/procesos/view_form' );
    }
    
    public function edit()
    {
        $this->layout->set( 'proceso', $this->procesos_model->get_procesos() );
        $this->load->library( 'form_validation' );
        $this->layout->set( 'form_action', 'edit' );
        if( $this->input->post( 'submit_proceso' ) ) {
            $this->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
            $this->form_validation->set_rules( 'proceso', 'Proceso', 'trim|required|min_length[5]|max_length[255]|xss_clean' );

            if( $this->form_validation->run() != FALSE ) {
                if( $this->procesos_model->save() ) {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_save' ) );
                    redirect( 'procesos/' );
                } else {
                    $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
                    redirect( 'procesos/' );
                }
            }
        }
        $this->layout->view( 'cumplimiento/procesos/view_form' );
    }

    public function delete()
    {
        if($this->procesos_model->delete()) {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_success_delete' ) );       
        } else {
            $this->session->set_flashdata( 'flashdata', $this->config->item( 'msg_error_delete' ) );
        }        
        redirect( 'procesos' );
    }    
    
}