<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( !function_exists( 'upload_document' ) ) {

    function upload_document( $form_filed_name )
    {
        //echo '<pre>'; print_r($_FILES); exit;
        $CI = & get_instance();
        if( isset( $_FILES[ $form_filed_name ][ 'tmp_name' ] ) ) {
            $CI->load->library( 'upload', $CI->config );
            if( $CI->upload->do_upload( $form_filed_name ) ) {
                $file = $CI->upload->data();
                $fp = fopen( $file[ 'full_path' ], 'rb' );
                $content = fread( $fp, filesize( $file[ 'full_path' ] ) );
                fclose( $fp );

                @unlink( $_FILES[ $form_filed_name ][ 'tmp_name' ] );
                @unlink( $file[ 'full_path' ] );

                return array(
                    'file_content' => base64_encode( $content ),
                    'file_type' => $file[ 'file_type' ],
                    'file_name' => $file[ 'client_name' ],
                    'file_size' => $file[ 'file_size' ]
                );
            } else {
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    if( !function_exists( 'printR' ) ) {
        function printR($vars, $exit=TRUE) 
        {
            echo '<pre>';
            print_r($vars);
            echo '</pre>';
            if($exit) {
                exit();
            }
        }
    }
    
    if( !function_exists( 'descargar_document' ) ) {
        function descargar_document($name, $type, $content) 
        {
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type:$type" );
            header( "Content-Disposition:attachment;filename=$name" );
            header( "Content-Transfer-Encoding: binary" );
            echo base64_decode( $content );
            exit();
        }
    }
    
    
    if( !function_exists( 'br2nbsp' ) ) {
        function br2nbsp($string)
        {
            return preg_replace('/\<br(\s*)?\/?\>/i', "", $string);
        }
    }
    
    
    if( !function_exists( 'quitar_acentos' ) ) {

        function quitar_acentos( $s )
        {
            $s = utf8_decode( $s );
            $s = preg_replace( "[áàâãª]", "a", $s );
            $s = preg_replace( "[ÁÀÂÃ]", "A", $s );
            $s = preg_replace( "[ÍÌÎ]", "I", $s );
            $s = preg_replace( "[íìî]", "i", $s );
            $s = preg_replace( "[éèê]", "e", $s );
            $s = preg_replace( "[ÉÈÊ]", "E", $s );
            $s = preg_replace( "[óòôõº]", "o", $s );
            $s = preg_replace( "[ÓÒÔÕ]", "O", $s );
            $s = preg_replace( "[úùû]", "u", $s );
            $s = preg_replace( "[ÚÙÛ]", "U", $s );
            $s = str_replace( "ç", "c", $s );
            $s = str_replace( "Ç", "C", $s );
            return $s;
        }

    }
    
    if(!function_exists( 'controles_ejecucion_array' )) {
        function controles_ejecucion_array()
        {
            return array( 'A' => 'Automático', 'M' => 'Manual' );
        }
    }
    
    if(!function_exists( 'controles_ejecucion_tipo_array' )) {
        function controles_ejecucion_tipo_array()
        {
            return array( 'P' => 'Preventivo', 'D' => 'Detectivo' );
        }
    }
    
    if(!function_exists( 'controles_frecuencias_array' )) {
        function controles_frecuencias_array()
        {
            return array(
                'A' => 'Anual',
                'S' => 'Semestral',
                'T' => 'Trimestral',
                'B' => 'Bimestral',
                'M' => 'Mensual',
                'Q' => 'Quincenal',
                'SM' => 'Semanal',
                'D' => 'Diaria',
                'C' => 'Continua'
            );
        }
    }
    
    if(!function_exists( 'ejecucion_color' )) {
        function ejecucion_color($key)
        {
            switch( $key ) {
                case 'A' : $bg = 'label label-success'; break;
                case 'M' : $bg = 'label label-inverse'; break;
            }
            return $bg;
        }
    }    
    
    if(!function_exists( 'tipo_color' )) {
        function tipo_color($key)
        {
            switch( $key ) {
                case 'P' : $bg = 'label label-warning'; break;
                case 'D' : $bg = 'label label-info'; break;
            }
            return $bg;
        }
    }    
    
    if(!function_exists( 'frecuencia_color' )) {
        function frecuencia_color($key)
        {
            switch( $key ) {
                case 'A' : $bg = 'label'; break;
                case 'S' : $bg = 'label label-success'; break;
                case 'T' : $bg = 'label label-warning'; break;
                case 'B' : $bg = 'label label-important'; break;
                case 'M' : $bg = 'label label-info'; break;
                case 'Q' : $bg = 'label label-inverse'; break;
                case 'SM' : $bg = 'label label-success'; break;
                case 'D' : $bg = 'label label-warning'; break;
                case 'C' : $bg = 'label label-important'; break;
            }
            return $bg;
        }
    }
    
    if(!function_exists( 'fecha_planificada_color' )) {
        function fecha_planificada_color($fecha)
        {
            if( ( $fecha - ( 86400 * 15 ) ) <= time() && $fecha > time() ) {
                $bg = 'label label-warning';
            } elseif( $fecha <= time() ) {
                $bg = 'label label-important';
            } else {
                $bg = 'label label-success';
            }
            return $bg;
        }
    }    
    
    if(!function_exists( 'riesgo_color' )) {
        function riesgo_color($key)
        {
            switch( $key ) {
                case 'A' : $bg = 'label label-important'; break;
                case 'B' : $bg = 'label label-info'; break;
                case 'M' : $bg = 'label label-success'; break;
            }
            return $bg;
        }
    }  
    
    if(!function_exists( 'estado_color' )) {
        function estado_color($key)
        {
            switch( $key ) {
                case 'AD' : $bg = 'label'; break;
                case 'EA' : $bg = 'label label-success'; break;
                case 'PL' : $bg = 'label label-warning'; break;
                case 'DS' : $bg = 'label label-important'; break;
                case 'SR' : $bg = 'label label-info'; break;
            }
            return $bg;
        }
    }  
    
    if(!function_exists( 'usuarios_perfiles_array' )) {
        function usuarios_perfiles_array($perfil = 'admin')
        {
            if( $perfil == 'admin' ) {
                return array( 'lector', 'editor', 'admin' );
            } elseif($perfil == 'desarrollador') {
                return array( 'lector', 'editor', 'admin', 'desarrollador' );
            }
        }
    }
    
    if(!function_exists( 'controllers_array' )) {
        function controllers_array()
        {
            $CI = & get_instance();
            $CI->load->helper('directory');            
            foreach( directory_map('application/controllers') as $controller ) {
                $arr[] = substr($controller, 0, -4);
            }
            return $arr;
        }
    }
    
    if( !function_exists( 'is_allowed' ) ) {

        function is_allowed( $controller = NULL, $action = NULL )
        {
            $CI = & get_instance();
            $CI->zend->load( 'Zend/Acl' );
            $CI->zend->load( 'Zend/Acl/Role' );
            $CI->zend->load( 'Zend/Acl/Resource' );

            $acl = new Zend_Acl();
            $auth = Zend_Session::namespaceGet( 'identify' );

            // CREATE RESOURCES
            foreach( controllers_array() as $contr ) {
                $acl->add( new Zend_Acl_Resource( $contr ) );
            }
            // CREATE ROLES
            foreach( usuarios_perfiles_array( 'desarrollador' ) as $perfil ) {
                if( $perfil == 'admin' ) {
                    // ADMIN HEREDA PERMISOS DE EDITOR
                    $acl->addRole( new Zend_Acl_Role( $perfil ), 'editor' );
                } else {
                    $acl->addRole( new Zend_Acl_Role( $perfil ) );
                }
            }
            // SET ACCESS RIGHTS
            $acl->deny( array( 'lector', 'editor', 'admin' ) );
            
            // LECTOR
            $acl->allow( 'lector', array( 'procesos', 'normas', 'procedimientos' ), array( 'index', 'proceso', 'descargar', 'logout' ) );
            
            // EDITOR
            //$acl->allow( 'editor', array( 'procesos', 'normas', 'procedimientos' ), array( 'index', 'proceso', 'descargar', 'logout', 'add', 'edit', 'delete' ) );
            $acl->allow( 'editor', 'procesos', array( 'index', 'add', 'edit', 'delete', 'proceso' ) );
            $acl->allow( 'editor', 'subprocesos', array( 'index', 'add', 'edit', 'delete', 'proceso' ) );
            $acl->allow( 'editor', 'normas', array( 'index', 'add', 'edit', 'delete', 'proceso', 'descargar' ) );
            $acl->allow( 'editor', 'procedimientos', array( 'index', 'add', 'edit', 'delete', 'proceso', 'descargar' ) );
            $acl->allow( 'editor', 'controles', array( 'add', 'edit', 'delete', 'descargar' ) );
            $acl->allow( 'editor', 'usuarios', array( 'login', 'logout' ) );
            
            // ADMIN
            $acl->allow( 'admin', 'usuarios', array( 'index', 'perfil' ) );
            $acl->allow( 'admin', 'observaciones', array( 'index', 'add', 'edit', 'delete' ) );
            $acl->allow( 'desarrollador' );
            
            if( !is_null( $controller ) ) {
                $resource = $controller;
            } else {
                $resource = $CI->controller ? $CI->controller : $CI->router->routes['default_controller'];
            }
            
            if( !is_null( $action ) ) {
                return $acl->isAllowed( $auth[ 'role' ], $resource, $action ) ? TRUE : FALSE;
            } else {
                return $acl->isAllowed( $auth[ 'role' ], $resource ) ? TRUE : FALSE;
            }
        }

    }
    
    if(!function_exists( 'control_style' )) {
        function control_style( $publicado, $plazo_min, $plazo_max )
        {
            $css_class = '';
            $min = strtotime("+".$plazo_min." months", $publicado);
            $max = strtotime("+".$plazo_max." months", $publicado);
            
            if( ( $publicado > $min ) && ( $publicado < $max ) ) {
                $css_class = 'label label-warning';
            } elseif( $publicado >= $max  ) {
                $css_class = 'label label-important';
            } else {
                $css_class = 'label label-success';
            }
            return $css_class;
        }
    }     
    
    if( !function_exists( 'fecha_format' ) ) {
        function fecha_format($str) 
        {
            return date("d/m", $str);
        }
    }    
    
}