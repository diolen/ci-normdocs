<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class BIND_Controller extends CI_Controller {
    
    public $controller;
    public $action;
    public $usuario;

    public function __construct() {
        parent::__construct();
        
        $this->zend->load('Zend/Session');
        Zend_Session::start();
        
        $this->layout->set_layout('layouts/layout_default');
        $this->layout->set_title('BIND Cumpliento 4609');

        $this->controller = $this->uri->segment(1) ? $this->uri->segment(1) : $this->router->default_controller;
        $this->action = $this->uri->segment(2) ? $this->uri->segment(2) : 'index';
        
        $this->layout->set('controller', $this->controller);
        $this->layout->set('action', $this->action);
        
        // PROCESOS Y SUBPROCESOS
        $this->proceso_id = $this->procesos_model->get_proceso_id();
        $this->subproceso_id = $this->subprocesos_model->get_subproceso_id();

        $this->layout->set( 'proceso_id', $this->proceso_id );
        $this->layout->set( 'subproceso_id', $this->subproceso_id );        
    }
    
    public function is_logged()
    {
        $auth = new Zend_Session_Namespace( 'identify' );

        if( !isset( $auth->username ) && !isset( $auth->ad_email ) ) {
            Zend_Session::destroy();
            redirect('usuarios/login');
        } else {
            $this->usuario = $auth;
            $this->layout->set('usuario', $this->usuario);
            return TRUE;            
        }

//        if($this->session->userdata('logged_in') != 'logged_in') {
//            redirect('usuarios/login');
//        }
    }

}
