<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BIND_Model extends CI_Model {
    
    protected $_CI;
    protected $_db;    
    
    public function __construct()
    {
        parent::__construct();
        
        /**
         * Init instance DB
         * Example
         * function test() 
         * {
         *     try {
         *         $sql = "SELECT * FROM active_directory_usuarios";
         *         $result = $this->db->fetchAll($sql);
         *     } catch (Zend_Db_Adapter_Exception $e) {
         *         echo $e->getMessage();
         *     } catch (Zend_Exception $e) {
         *         echo $e->getMessage();
         *     } catch(Exception $e) {
         *         echo $e->getMessage();
         *     }        
         * }
         */
        $this->zend->load('Zend/Db');
        $this->db_cumplimiento = Zend_Db::factory('Sqlsrv', $this->config->item('db_cumplimiento'));
        
        $this->_CI = & get_instance();
        $this->_db = $this->db_cumplimiento;
    }

}