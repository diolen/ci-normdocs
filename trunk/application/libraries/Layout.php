<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout {

    private $layout_vars = array();
    private $vars = array();
    private $layout = 'charisma/layout_charisma';
    private $title = 'Charisma';

    function set_layout($template)
    {
        $this->layout = $template;
    }

    function set_title($title)
    {
        $this->title = $title;
    }

    function set($var_name, $value)
    {
        $this->vars[$var_name] = $value;
    }

    function set_global($var_name, $value)
    {
        $this->layout_vars[$var_name] = $value;
    }

    function fetch($template)
    {
        $CI = &get_instance();
        $content = $CI->load->view($template, $this->vars, true);
        $this->layout_vars['content'] = $content;
        $this->layout_vars['title'] = $this->title;
        return $CI->load->view($this->layout, $this->layout_vars, true);
    }

    function view($template)
    {
        echo $this->fetch($template);
    }

}