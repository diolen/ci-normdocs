<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subprocesos_Model extends BIND_Model {

    protected $_table_normas = 'cumplimientos_normas';
    protected $_table_procedimientos = 'cumplimientos_procedimientos';
    protected $_table_procedimientos_controles = 'cumplimientos_procedimientos_controles';
    protected $_table_subprocesos = 'cumplimientos_subprocesos';
    protected $_error_message = "";
    
    public $proceso_id;
    public $subproceso_id;

    public function __construct() 
    {
        parent::__construct();
        $this->subproceso_id = $this->get_subproceso_id();
        $this->proceso_id = $this->_CI->procesos_model->get_proceso_id();
    }
    
    public function get_subproceso_id()
    {
        return $this->_CI->input->post('subproceso_id')?$this->_CI->input->post('subproceso_id'):$this->_CI->uri->segment(4); 
    }    
    
    public function get_subprocesos()
    {
        $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
        if( !empty( $this->subproceso_id ) ) {
            $sql = "SELECT * FROM $this->_table_subprocesos WHERE subproceso_id = ". $this->subproceso_id;
            return $this->_db->fetchRow( $sql );
        } else {
            $sql = "SELECT * FROM $this->_table_subprocesos WHERE proceso_id = ". $this->proceso_id;
            return $this->_db->fetchAll( $sql );
        }
    }  
    
    public function save()
    {
        $this->_db->beginTransaction();
        try {
            if( $this->_CI->input->post( 'form_action' ) == 'add' ) {
                $args = array( 
                    'proceso_id' => $this->_CI->procesos_model->get_proceso_id(), 
                    'subproceso_nombre' => strip_tags( $this->_CI->input->post( 'subproceso' ) ), 
                    'fecha_alta' => time() );
                $this->_db->insert( $this->_table_subprocesos, $args ); 
                $this->_db->commit();
                return $this->_db->lastInsertId();
            }
            
            if( $this->_CI->input->post( 'form_action' ) == 'edit' ) {
                $args = array( 
                    'subproceso_nombre' => strip_tags( $this->_CI->input->post( 'subproceso' ) ), 
                    'fecha_mod' => time() );
                $this->_db->update( $this->_table_subprocesos, $args, 'subproceso_id = '. $this->subproceso_id );
                $this->_db->commit();
                return TRUE;
            }
            
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;                
        }                
    }
        
    public function delete()
    {
        $this->_db->beginTransaction();
        try {
            $sql = "";
            $sql .= "DELETE FROM $this->_table_procedimientos_controles WHERE procedimiento_id IN ( SELECT procedimiento_id FROM $this->_table_procedimientos WHERE subproceso_id = $this->subproceso_id )";
            $sql .= "DELETE FROM $this->_table_procedimientos WHERE subproceso_id = '. $this->subproceso_id )";
            $sql .= "DELETE FROM $this->_table_normas WHERE subproceso_id = '. $this->subproceso_id )";
            $sql .= "DELETE FROM $this->_table_subprocesos WHERE subproceso_id = '. $this->subproceso_id )";
            $this->_db->query( $sql );            
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;                
        }        
    }    
}