<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_Model extends BIND_Model {

    protected $displayname_excluidos = array(
        'Usuario Invitado',
        'Internet Guest Account',
        'TsInternetUser',
        'dbas',
        'Sobre A22.3',
        'Usuario Servicio para ejecucion de Jobs en BD',
        'sac',
        'scanner',
        'srvcinvgate',
        'srvcgapp',
        'srvcbtprint',
        '___VMware_Conv_SA___',
        'db_smartopen_dr',
        'srvcbindcheck',
        'srvcosadm',
        'srvcqvprd',
        'srvcsapportal',
        'srvcsatcs',
        'srvcsapportalqa',
        'srvcsapportalprd',
        'srvcprintsrv',
        'srvcsmartopen',
        'testpap',
        'usrclasifica',
        'srvcaltiris',
        'Test Genexus',
        'db_qlikview_so',
        'srvc_qv_des',
        'srvc_caja_db_qa',
        'srvc_caja_db_des',
        'srvc_caja_db_prd',
        'srvcofficescan',
        'Operador MDA',
        'Bantotal',
        'bantotal1',
        'bantotal2',
        'bantotal3',
        'bantotal4',
        'bantotal5',
        'bantotalsi',
        'srvcbkpexecqa',
        'cajero 1',
        'Cajero 2',
        'Tesorero',
        'Tesorero 1',
        'nucleocon',
        'nucleo',
        'pasivascon',
        'pasivas',
        'activascon',
        'activas',
        'backup',
        'fortinet',
        'Sobre A22',
        'sqlserver',
        'sacadmin',
        'admin',
        'adminusr',
        'adminsso',
        'adminleasing',
        'adminsatcs',
        'datastage',
        'sapserv',
        'usersiopel',
        'ADSelfService',
        'adminsos',
        'VMWare Servicio',
        'Authentication Services',
        'Seguridad Policia Federal Argentina',
        'Administracor PAP'
    );
    
    public $usuarios;
    public $perfiles;
    protected $_table_usuarios = 'active_directory_usuarios';
    protected $_table_usuarios_perfiles = 'cumplimientos_usuarios_perfiles';

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_users($id = NULL)
    {
        $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
        if( !is_null( $id ) ) {
            $sql = "SELECT * FROM $this->_table_usuarios WHERE id_usuario = $id";
            $this->usuarios = $this->_db->fetchRow( $sql );
        } else {
            $sql = "SELECT * FROM $this->_table_usuarios";
            $this->usuarios = $this->_db->fetchAll( $sql );
        }        
        return $this->usuarios;
    }

    public function login( $user_search, $user_logged, $pass_logged )
    {
        $CI = &get_instance();
        $auth = $CI->ldap->authenticate( $user_logged, $pass_logged );
        if( $auth[ 'ok' ] ) {
            try {
                $ad = $CI->ldap->user_info( strtolower( $user_search ) );
                $displayname = str_replace( ' - EXT', '', $ad[ 0 ][ 'displayname' ][ 0 ] );
                $ad_displayname = isset( $displayname ) ? ucwords( strtolower( $displayname ) ) : 'No definido';
                $ad_telefono_interno = isset( $ad[ 0 ][ 'telephonenumber' ][ 0 ] ) ? $ad[ 0 ][ 'telephonenumber' ][ 0 ] : 'No definido';
                $ad_email = isset( $ad[ 0 ][ 'mail' ][ 0 ] ) ? $ad[ 0 ][ 'mail' ][ 0 ] : 'No definido';
                $this->get_perfiles( $user_logged );
                $perfiles_array = usuarios_perfiles_array('desarrollador');
                
                $auth = new Zend_Session_Namespace( 'identify' );
                $auth->logged_in = 'logged_in';
                $auth->username = $CI->input->post( 'username' );
                $auth->password = $CI->input->post( 'password' );
                $auth->ad_displayname = $ad_displayname;
                $auth->ad_telefono_interno = $ad_telefono_interno;
                $auth->ad_email = $ad_email;
                $auth->role = !empty($this->perfiles->perfil) ? $perfiles_array[$this->perfiles->perfil] : 'lector';

            } catch( Exception $e ) {
                return FALSE;
            }
        }
        return TRUE;
    }

    public function db_sincronizar()
    {
        $this->_db->beginTransaction();
        try {
            $auth = Zend_Session::namespaceGet( 'identify' );
            $user_logged = $auth['username'];
            $pass_logged = $auth['password'];            
            $auth = $this->ldap->authenticate( $user_logged, $pass_logged );
            if( $auth[ 'ok' ] ) {
                $str = "INSERT INTO active_directory_usuarios( ad_usuario, ad_telefono_interno, ad_email, ad_displayname, ad_name, ad_lastname ) VALUES( ";
                foreach( $this->ldap->users_info_advanced2() as $usuarios ) {
                    if( !empty( $usuarios[ 'displayname' ] ) && !in_array( $usuarios[ 'displayname' ], $this->displayname_excluidos ) ) {
                        $str .= "'". $usuarios[ 'username' ] ."', '". $usuarios[ 'int' ] ."', '". $usuarios[ 'email' ] ."', '". $usuarios[ 'displayname' ] ."', '". $usuarios[ 'name' ] ."', '". $usuarios[ 'lastname' ] . "'), (";
                    }
                }
                $sql = substr( $str, 0, -3 ) . ";";                
                $this->_db->query( "TRUNCATE TABLE $this->_table_usuarios" );
                $this->_db->query( $sql );
                $this->_db->commit();
                return true;                
            }
        }catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }
    
    public function save_perfil()
    {
        $this->_db->beginTransaction();
        try {
            $usuario = $this->_CI->input->post( 'usuario' );
            $perfil = $this->_CI->input->post( 'perfil' );
            if( self::get_perfiles( $usuario ) ) {
                $this->_db->delete( $this->_table_usuarios_perfiles, "usuario = '". $usuario ."'" );
            }
            $args = array( 'usuario' => $usuario, 'perfil' => $perfil );
            $this->_db->insert( $this->_table_usuarios_perfiles, $args );
            $this->_db->commit();
            return $this->_db->lastInsertId();
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    public function get_perfiles($usuario = NULL)
    {
        try {
            $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
            if( !is_null( $usuario ) && preg_match( "/^[a-z]*$/", trim($usuario ) ) )  {
                $sql = "SELECT * FROM $this->_table_usuarios_perfiles WHERE usuario = '". $usuario ."'";
                return $this->perfiles = $this->_db->fetchRow( $sql );
            } else {
                $sql = "SELECT * FROM $this->_table_usuarios_perfiles";
                return $this->perfiles = $this->_db->fetchAll( $sql );
            }
            return $this->usuarios;
        } catch( Zend_Db_Adapter_Exception $e ) {
            return FALSE;
        }
    }

    public function delete_perfil()
    {
        $this->_db->beginTransaction();
        try {
            $usuario = $this->_CI->input->post( 'usuario' );
            $this->_db->delete( $this->_table_usuarios_perfiles, "usuario = '". $usuario ."'" );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

}
    