<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controles_Model extends BIND_Model {

    protected $_table_controles = 'cumplimientos_procedimientos_controles';
    protected $_error_message = "";
    
    public $ejecucion_ddl;
    public $ejecucion_tipo_ddl;
    public $frecuencia_ddl;
    public $procedimiento_id_id;
    public $control_id;
    public $control;
    public $controles;

    public function __construct()
    {
        parent::__construct();
        $this->control_id = $this->get_control_id();
        $this->procedimiento_id = $this->procedimientos_model->get_procedimiento_id();
    }

    public function get_control_id()
    {
        return $this->_CI->input->post( 'control_id' ) ? $this->_CI->input->post( 'control_id' ) : $this->_CI->uri->segment( 6 );
    }

    public function get_controles()
    {
        try {
            $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
            if( !empty( $this->control_id ) ) {
                $sql = "SELECT * FROM $this->_table_controles WHERE control_id = $this->control_id";
                $this->control = $this->_db->fetchRow( $sql );
                return $this->control;
            } else {
                $sql = "SELECT * FROM $this->_table_controles";
                $this->controles = $this->_db->fetchAll( $sql );
                return $this->controles;            
            }
        } catch( Zend_Db_Adapter_Exception $e ) {
            return FALSE;
        }
    }

    public function save()
    {
        $this->_db->beginTransaction();
        try {
            // DOCUMENT UPLOAD
            $document = !empty( $_FILES[ 'control_doc' ][ 'tmp_name' ] ) ? upload_document( 'control_doc' ) : '';
            $evidencia_document = !empty( $_FILES[ 'control_evidencia_doc' ][ 'tmp_name' ] ) ? upload_document( 'control_evidencia_doc' ) : '';
            $args = $this->save_prepare( $this->_CI->input->post(), $document, $evidencia_document );
            
            if( $this->_CI->input->post( 'form_action' ) == 'add' ) {
                $this->_db->insert( $this->_table_controles, $args );
                $this->_db->commit();
                return $this->_db->lastInsertId();
            } elseif( $this->_CI->input->post( 'form_action' ) == 'edit' ) {
                $this->_db->update( $this->_table_controles, $args, 'control_id = ' . $this->control_id );
                $this->_db->commit();
                return TRUE;
            }
        } catch (Zend_Db_Adapter_Exception $e) {
            $this->_db->rollBack();
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
        } catch(Exception $e) {
            $this->_db->rollBack();
        }          
    }

    private function save_prepare( $post_data, $doc, $evidencia_doc )
    {
        if( !empty( $doc ) ) {
            $args[ 'control_doc' ] = $doc[ 'file_content' ];
            $args[ 'control_doc_nombre' ] = $doc[ 'file_name' ];
            $args[ 'control_doc_type' ] = $doc[ 'file_type' ];
        }

        if( !empty( $evidencia_doc ) ) {
            $args[ 'control_evidencia_doc' ] = $evidencia_doc[ 'file_content' ];
            $args[ 'control_evidencia_doc_nombre' ] = $evidencia_doc[ 'file_name' ];
            $args[ 'control_evidencia_doc_type' ] = $evidencia_doc[ 'file_type' ];
        }

        if( $post_data[ 'form_action' ] == 'add' ) {
            $args[ 'fecha_alta' ] = time();
        } elseif( $post_data[ 'form_action' ] == 'edit' ) {
            $args[ 'fecha_mod' ] = time();
        }

        $args[ 'procedimiento_id' ] = $this->procedimiento_id;
        $args[ 'control_titulo' ] = strip_tags( trim( $post_data[ 'control_titulo' ] ) );
        $args[ 'control_ejecucion' ] = $post_data[ 'control_ejecucion' ];
        $args[ 'control_tipo' ] = $post_data[ 'control_tipo' ];
        $args[ 'control_frecuencia' ] = $post_data[ 'control_frecuencia' ];
        $args[ 'control_evidencia' ] = strip_tags( trim( $post_data[ 'control_evidencia' ] ) );
        //printR($args);
        return $args;
    }

    public function get_error_message( $action = "" )
    {
        switch( $action ) {
            case 'add' :
                if( $this->_CI->upload->display_errors() ) {
                    $this->_error_message = '<div class="error">' . $this->_CI->upload->display_errors() . '</div>';
                } else {
                    $this->_error_message = $this->config->item( 'msg_error_save' );
                }
                break;
            case 'delete' :
                $this->_error_message = $this->config->item( 'msg_error_delete' );
                break;
            default :
                $this->_error_message = $this->config->item( 'msg_error_app' );
                break;
        }
        return $this->_error_message;
    }

    public function delete()
    {
        $this->_db->beginTransaction();
        try {
            $this->_db->delete( $this->_table_controles, 'control_id = ' . $this->control_id );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    public function get_document()
    {
        $doc = self::get_controles();
        if( strpos( uri_string(), 'evidencia' ) !== FALSE ) {
            descargar_document( $doc->control_evidencia_doc_nombre, $doc->control_evidencia_doc_type, $doc->control_evidencia_doc );
        } else {
            descargar_document( $doc->control_doc_nombre, $doc->control_doc_type, $doc->control_doc );
        }
    }

    public function delete_document()
    {
        $this->_db->beginTransaction();
        try {
            if( strpos( uri_string(), 'evidencia' ) !== FALSE ) {
                $args = array( 'control_evidencia_doc' => '', 'control_evidencia_doc_nombre' => '', 'control_evidencia_doc_type' => '' );
            } else {
                $args = array( 'control_doc' => '', 'control_doc_nombre' => '', 'control_doc_type' => '' );
            }
            $this->_db->update( $this->_table_controles, $args, 'control_id = ' . $this->control_id );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }
    
    public function get_ejecucion_ddl() 
    {
        return $this->ejecucion_ddl = controles_ejecucion_array(); 
    }

    public function get_ejecucion_tipo_ddl() 
    {
        return $this->ejecucion_tipo_ddl = controles_ejecucion_tipo_array(); 
    }

    public function get_frecuencia_ddl()
    {
        return $this->frecuencia_ddl = controles_frecuencias_array();
    }
    
    public function get_ejecucion_seleccionada($form_action)
    {
        if($form_action == 'add') {
            $control_ejecucion = NULL;
        } else {
            $control_ejecucion = $this->control->control_ejecucion;            
        }
        return $this->_CI->input->post( 'control_ejecucion' ) ? $this->_CI->input->post( 'control_ejecucion' ) : $control_ejecucion;
    }

    public function get_tipo_seleccionado($form_action)
    {
        if($form_action == 'add') {
            $tipo = NULL;
        } else {
            $tipo = $this->control->control_tipo;            
        }
        return $this->_CI->input->post( 'control_tipo' ) ? $this->_CI->input->post( 'control_tipo' ) : $tipo;
    }

    public function get_frecuencia_seleccionada($form_action)
    {
        if($form_action == 'add') {
            $frecuencia = NULL;
        } else {
            $frecuencia = $this->control->control_frecuencia;            
        }
        return $this->_CI->input->post( 'control_frecuencia' ) ? $this->_CI->input->post( 'control_frecuencia' ) : $frecuencia;
    }
    
    public function set_combos_valores_seleccionados($form_action)
    {
        $this->_CI->layout->set( 'ejecucion_seleccionada', $this->get_ejecucion_seleccionada( $form_action ) );
        $this->_CI->layout->set( 'tipo_seleccionado', $this->get_tipo_seleccionado( $form_action ) );
        $this->_CI->layout->set( 'frecuencia_seleccionada', $this->get_frecuencia_seleccionada( $form_action ) );
    }
    
    public function valid_form()
    {
        $this->_CI->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
        $this->_CI->form_validation->set_rules( 'control_titulo', 'Control', 'trim|required|min_length[5]|max_length[255]|xss_clean' );
        $this->_CI->form_validation->set_rules( 'control_evidencia', 'Evidencia', 'trim|required|min_length[5]|max_length[255]|xss_clean' );
    }

}
