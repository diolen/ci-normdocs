<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Normas_Model extends BIND_Model {

    protected $_table_normas = 'cumplimientos_normas';
    protected $_error_message = "";
    public $norma_id;
    public $subproceso_id;

    public function __construct()
    {
        parent::__construct();

        $this->norma_id = $this->get_norma_id();
        $this->subproceso_id = $this->_CI->subprocesos_model->get_subproceso_id();
    }

    public function get_norma_id()
    {
        return $this->_CI->input->post( 'norma_id' ) ? $this->_CI->input->post( 'norma_id' ) : $this->_CI->uri->segment( 5 );
    }

    public function get_normas()
    {
        $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
        if( !empty( $this->norma_id ) ) {
            $sql = "SELECT * FROM $this->_table_normas WHERE norma_id = $this->norma_id";
            return $this->_db->fetchRow( $sql );
        } else {
            $sql = "SELECT * FROM $this->_table_normas WHERE subproceso_id = " . $this->subproceso_id;
            return $this->_db->fetchAll( $sql );
        }
    }

    public function save()
    {
        $this->_db->beginTransaction();
        try {
            // DOCUMENT UPLOAD
            $document = !empty( $_FILES[ 'norma_doc' ][ 'tmp_name' ] ) ? upload_document( 'norma_doc' ) : '';
            $args = $this->save_prepare( $this->_CI->input->post(), $document );

            if( $this->_CI->input->post( 'form_action' ) == 'add' ) {
                $this->_db->insert( $this->_table_normas, $args );
                $this->_db->commit();
                return $this->_db->lastInsertId();
            } elseif( $this->_CI->input->post( 'form_action' ) == 'edit' ) {
                $this->_db->update( $this->_table_normas, $args, 'norma_id = ' . $this->get_norma_id() );
                $this->_db->commit();
                return TRUE;
            }
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    private function save_prepare( $post_data, $upload_data )
    {
        if( !empty( $upload_data ) ) {
            $args[ 'norma_doc' ] = $upload_data[ 'file_content' ];
            $args[ 'norma_doc_nombre' ] = $upload_data[ 'file_name' ];
            $args[ 'norma_doc_type' ] = $upload_data[ 'file_type' ];
        }

        if( $post_data[ 'form_action' ] == 'add' ) {
            $args[ 'fecha_alta' ] = time();
        } elseif( $post_data[ 'form_action' ] == 'edit' ) {
            $args[ 'fecha_mod' ] = time();
        }

        $args[ 'subproceso_id' ] = $this->subproceso_id;
        $args[ 'norma_titulo' ] = strip_tags( $post_data[ 'norma_titulo' ] );
        $args[ 'norma_req' ] = nl2br( strip_tags( $post_data[ 'norma_requerimientos' ] ) );
        return $args;
    }

    public function get_error_message( $action = "" )
    {
        switch( $action ) {
            case 'add' :
                if( $this->_CI->upload->display_errors() ) {
                    $this->_error_message = '<div class="error">' . $this->_CI->upload->display_errors() . '</div>';
                } else {
                    $this->_error_message = $this->config->item( 'msg_error_save' );
                }
                break;
            case 'delete' :
                $this->_error_message = $this->config->item( 'msg_error_delete' );
                break;
            default :
                $this->_error_message = $this->config->item( 'msg_error_app' );
                break;
        }
        return $this->_error_message;
    }

    public function delete()
    {
        $this->_db->beginTransaction();
        try {
            $this->_db->delete( $this->_table_normas, 'norma_id = ' . $this->norma_id );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    public function get_document()
    {
        $doc = self::get_normas( $this->subproceso_id );
        descargar_document( $doc->norma_doc_nombre, $doc->norma_doc_type, $doc->norma_doc );
    }

    public function delete_document()
    {
        $this->_db->beginTransaction();
        try {
            $args = array( 'norma_doc' => '', 'norma_doc_nombre' => '', 'norma_doc_type' => '' );
            $this->_db->update( $this->_table_normas, $args, 'norma_id = ' . $this->get_norma_id() );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

}
