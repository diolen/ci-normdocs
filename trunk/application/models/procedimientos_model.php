<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procedimientos_Model extends BIND_Model {

    protected $_table_procedimientos = 'cumplimientos_procedimientos';
    protected $_table_procedimientos_controles = 'cumplimientos_procedimientos_controles';
    protected $_error_message = "";
    public $procedimiento_id;
    public $procedimiento;
    public $procedimientos;

    public function __construct()
    {
        parent::__construct();

        $this->procedimiento_id = $this->get_procedimiento_id();
        $this->subproceso_id = $this->_CI->subprocesos_model->get_subproceso_id();
    }

    public function get_procedimiento_id()
    {
        return $this->_CI->input->post( 'procedimiento_id' ) ? $this->_CI->input->post( 'procedimiento_id' ) : $this->_CI->uri->segment( 5 );
    }

    public function get_procedimientos()
    {
        try {
            $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
            if( !empty( $this->procedimiento_id ) ) {
                $sql = "SELECT * FROM $this->_table_procedimientos WHERE procedimiento_id = $this->procedimiento_id";
                $this->procedimiento = $this->_db->fetchRow( $sql );
                return $this->procedimiento;
            } else {
                $sql = "SELECT * FROM $this->_table_procedimientos WHERE subproceso_id = " . $this->subproceso_id;
                $this->procedimientos = $this->_db->fetchAll( $sql );
                return $this->procedimientos;
            }
        } catch( Zend_Db_Adapter_Exception $e ) {
            return FALSE;
        }
    }

    public function save()
    {
        $this->_db->beginTransaction();
        try {
            // DOCUMENT UPLOAD
            $document = !empty( $_FILES[ 'procedimiento_doc' ][ 'tmp_name' ] ) ? upload_document( 'procedimiento_doc' ) : '';
            $args = $this->save_prepare( $this->_CI->input->post(), $document );

            if( $this->_CI->input->post( 'form_action' ) == 'add' ) {
                $this->_db->insert( $this->_table_procedimientos, $args );
                $this->_db->commit();
                return $this->_db->lastInsertId();
            } elseif( $this->_CI->input->post( 'form_action' ) == 'edit' ) {
                $this->_db->update( $this->_table_procedimientos, $args, 'procedimiento_id = ' . $this->procedimiento_id );
                $this->_db->commit();
                return TRUE;
            }
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    private function save_prepare( $post_data, $doc )
    {
        if( !empty( $doc ) ) {
            $args[ 'procedimiento_doc' ] = $doc[ 'file_content' ];
            $args[ 'procedimiento_doc_nombre' ] = $doc[ 'file_name' ];
            $args[ 'procedimiento_doc_type' ] = $doc[ 'file_type' ];
        }

        if( $post_data[ 'form_action' ] == 'add' ) {
            $args[ 'fecha_alta' ] = time();
        } elseif( $post_data[ 'form_action' ] == 'edit' ) {
            $args[ 'fecha_mod' ] = time();
        }

        $args[ 'subproceso_id' ] = $this->subproceso_id;
        $args[ 'procedimiento_titulo' ] = strip_tags( $post_data[ 'procedimiento_titulo' ] );
        $args[ 'procedimiento_req' ] = nl2br( strip_tags( $post_data[ 'procedimiento_requerimientos' ] ) );
        return $args;
    }

    public function get_error_message( $action = "" )
    {
        switch( $action ) {
            case 'add' :
                if( $this->_CI->upload->display_errors() ) {
                    $this->_error_message = '<div class="error">' . $this->_CI->upload->display_errors() . '</div>';
                } else {
                    $this->_error_message = $this->config->item( 'msg_error_save' );
                }
                break;
            case 'delete' :
                $this->_error_message = $this->config->item( 'msg_error_delete' );
                break;
            default :
                $this->_error_message = $this->config->item( 'msg_error_app' );
                break;
        }
        return $this->_error_message;
    }

    public function delete()
    {
        $this->_db->beginTransaction();
        try {
            $this->_db->delete( $this->_table_procedimientos_controles, 'procedimiento_id = ' . $this->procedimiento_id );
            $this->_db->delete( $this->_table_procedimientos, 'procedimiento_id = ' . $this->procedimiento_id );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    public function get_document()
    {
        $doc = self::get_procedimientos( $this->subproceso_id );
        descargar_document( $doc->procedimiento_doc_nombre, $doc->procedimiento_doc_type, $doc->procedimiento_doc );
    }

    public function delete_document()
    {
        $this->_db->beginTransaction();
        try {
            $args = array( 'procedimiento_doc' => '', 'procedimiento_doc_nombre' => '', 'procedimiento_doc_type' => '' );
            $this->_db->update( $this->_table_procedimientos, $args, 'procedimiento_id = ' . $this->procedimiento_id );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }

    public function valid_form()
    {
        $this->_CI->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
        $this->_CI->form_validation->set_rules( 'procedimiento_titulo', 'Titulo', 'trim|required|min_length[5]|max_length[255]|xss_clean' );
        $this->_CI->form_validation->set_rules( 'procedimiento_requerimientos', 'Requerimientos', 'trim|required|min_length[5]|xss_clean' );
    }

}
