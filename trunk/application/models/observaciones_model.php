<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Observaciones_Model extends BIND_Model {
    
    protected $_table_observaciones = 'cumplimientos_observaciones';
    protected $observacion_id;

    public function __construct()
    {
        parent::__construct();
        $this->observacion_id = $this->set_observacion_id();
    }    
    
    public function set_observacion_id()
    {
        return $this->_CI->input->post( 'observ_id' ) ? $this->_CI->input->post( 'observ_id' ) : $this->_CI->uri->segment( 3 );
    }

    public function get_observaciones()
    {
        $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
        if( !empty( $this->observacion_id ) ) {
            $sql = "SELECT * FROM $this->_table_observaciones WHERE observ_id = $this->observacion_id";
            return $this->_db->fetchRow( $sql );
        } else {
            $sql = "SELECT * FROM $this->_table_observaciones";
            return $this->_db->fetchAll( $sql );
        }
    }    
    
    public function save()
    {
        $this->_db->beginTransaction();
        try {
            $args = $this->save_prepare( $this->_CI->input->post() );

            if( $this->_CI->input->post( 'form_action' ) == 'add' ) {
                $this->_db->insert( $this->_table_observaciones, $args );
                $this->_db->commit();
                return $this->_db->lastInsertId();
            } elseif( $this->_CI->input->post( 'form_action' ) == 'edit' ) {
                $this->_db->update( $this->_table_observaciones, $args, 'observ_id = ' . $this->observacion_id );
                $this->_db->commit();
                return TRUE;
            }
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }    
    
    private function save_prepare( $data )
    {
        if( $data[ 'form_action' ] == 'add' ) {
            $args[ 'fecha_alta' ] = time();
        } elseif( $data[ 'form_action' ] == 'edit' ) {
            $args[ 'fecha_mod' ] = time();
        }
        
        $fecha_planificada = explode( '/', $data[ 'observ_fecha_planificada' ] );
        $args[ 'observ_indentificacion' ] = strip_tags( $data[ 'observ_indentificacion' ] );
        $args[ 'observ_origen_auditoria' ] = $data[ 'observ_origen_auditoria' ];
        $args[ 'observ_origen_ano' ] = $data[ 'observ_origen_ano' ];
        $args[ 'observ_observacion' ] = nl2br( strip_tags( $data[ 'observ_observacion' ] ) );
        $args[ 'observ_tematica' ] = strip_tags( $data[ 'observ_tematica' ] );
        $args[ 'observ_estado' ] = $data[ 'observ_estado' ];
        $args[ 'observ_riesgo' ] = $data[ 'observ_riesgo' ];
        $args[ 'observ_respuesta' ] = nl2br( strip_tags( $data[ 'observ_respuesta' ] ) );
        $args[ 'observ_responsable' ] = nl2br( strip_tags( $data[ 'observ_responsable' ] ) );
        $args[ 'observ_fecha_planificada' ] = strtotime( $fecha_planificada[2] .'-'. $fecha_planificada[1] .'-'. $fecha_planificada[0] .' 00:00:00' );
        return $args;
    }   
    
    public function delete()
    {
        $this->_db->beginTransaction();
        try {
            $this->_db->delete( $this->_table_observaciones, 'observ_id = ' . $this->observacion_id );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }    
    
    public function auditoria_ddl()
    {
        return array(
            'BCRA' => 'Banco Central de la República Argentina',
            'AI' => 'Auditoría Interna',
            'AE' => 'Auditoría Externa'
        );
    }
    
    public function ano_ddl() {
        $start = '2005';
        for($i=0; $i < 15; $i++) {
            $ano[$start] = $start;
            $start++;
        }
        return $ano;
    }
    
    public function estado_ddl()
    {
        return array(
            'AD' => 'Adecuada',
            'EA' => 'En adecuación',
            'PL' => 'Planificada',
            'DS' => 'Desestimada',
            'SR' => 'Sujeta a revisión'
        );
    }    

    public function riesgo_ddl()
    {
        return array(
            'A' => 'Alto',
            'M' => 'Medio',
            'B' => 'Baja'
        );
    } 
    
    public function valid_form()
    {
        $this->_CI->form_validation->set_error_delimiters( '<br><span class="error">', '</span>' );
        $this->_CI->form_validation->set_rules( 'observ_indentificacion', 'ID', 'trim|required|min_length[2]|max_length[255]|xss_clean' );
        $this->_CI->form_validation->set_rules( 'observ_observacion', 'Observación', 'trim|xss_clean' );
        $this->_CI->form_validation->set_rules( 'observ_tematica', 'Temática', 'trim|xss_clean' );
        $this->_CI->form_validation->set_rules( 'observ_respuesta', 'Respuesta', 'trim|xss_clean' );
        $this->_CI->form_validation->set_rules( 'observ_responsable', 'Responsable', 'trim|xss_clean' );
        $this->_CI->form_validation->set_rules( 'observ_fecha_planificada', 'Responsable', 'trim|required|xss_clean' );
    }
    

}