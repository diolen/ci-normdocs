<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procesos_Model extends BIND_Model {

    protected $_table_normas = 'cumplimientos_normas';
    protected $_table_procedimientos = 'cumplimientos_procedimientos';
    protected $_table_procedimientos_controles = 'cumplimientos_procedimientos_controles';
    protected $_table_subprocesos = 'cumplimientos_subprocesos';    
    protected $_table_procesos = 'cumplimientos_procesos';
    protected $_error_message = "";

    public function __construct() 
    {
        parent::__construct();
        $this->proceso_id = $this->get_proceso_id();
    }
    
    public function get_proceso_id()
    {
        return $this->_CI->input->post('proceso_id')?$this->_CI->input->post('proceso_id'):$this->_CI->uri->segment(3); 
    }    
    
    public function get_procesos()
    {
        $this->_db->setFetchMode( Zend_Db::FETCH_OBJ );
        if( !empty( $this->proceso_id ) ) {
            $sql = "SELECT * FROM $this->_table_procesos WHERE proceso_id = $this->proceso_id";
            return $this->_db->fetchRow( $sql );
        } else {
            $sql = "SELECT * FROM $this->_table_procesos";
            return $this->_db->fetchAll( $sql );
        }
    }  
    
    public function save()
    {
        $this->_db->beginTransaction();
        try {
            if( $this->_CI->input->post( 'form_action' ) == 'add' ) {
                $args = array( 'proceso_nombre' => strip_tags( $this->_CI->input->post( 'proceso' ) ), 'fecha_alta' => time() );
                $this->_db->insert( $this->_table_procesos, $args ); 
                $this->_db->commit();
                return $this->_db->lastInsertId();
            }
            
            if( $this->_CI->input->post( 'form_action' ) == 'edit' ) {
                $args = array( 'proceso_nombre' => strip_tags( $this->_CI->input->post( 'proceso' ) ), 'fecha_mod' => time() );
                $this->_db->update( $this->_table_procesos, $args, 'proceso_id = '. $this->proceso_id );
                $this->_db->commit();
                return TRUE;
            }
            
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;                
        }                
    }
        
    public function delete()
    {
        $sql = "";
        $this->_db->beginTransaction();
        try {
            // Eliminación en cascada 
            // Procesos->Subprocesos->Normas->Procedimientos->Controles
            $sql .= "DELETE FROM $this->_table_procedimientos_controles  WHERE procedimiento_id IN ( SELECT procedimiento_id FROM $this->_table_procedimientos WHERE subproceso_id IN ( SELECT subproceso_id FROM $this->_table_subprocesos WHERE proceso_id = $this->proceso_id ) )";
            $sql .= "DELETE FROM $this->_table_procedimientos WHERE subproceso_id IN (SELECT subproceso_id FROM $this->_table_subprocesos WHERE proceso_id = $this->proceso_id)";
            $sql .= "DELETE FROM $this->_table_normas WHERE subproceso_id IN (SELECT subproceso_id FROM $this->_table_subprocesos WHERE proceso_id = $this->proceso_id)";
            $sql .= "DELETE FROM $this->_table_subprocesos WHERE proceso_id = '. $this->proceso_id";
            $sql .= "DELETE FROM $this->_table_procesos WHERE proceso_id = '. $this->proceso_id";
            $this->_db->query( $sql );
            $this->_db->commit();
            return TRUE;
        } catch( Zend_Db_Adapter_Exception $e ) {
            $this->_db->rollBack();
            return FALSE;
        }
    }    
}