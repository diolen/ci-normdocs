<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['upload_path'] = './uploads/';
$config['allowed_types'] = 'gif|jpg|png|ppt|pptx|doc|docx|txt|pdf|xls|zip|rar|odt|ods|csv';
$config['max_size'] = '2048';
$config['max_width'] = '0';
$config['max_height'] = '0';
$config['remove_spaces'] = 'true';