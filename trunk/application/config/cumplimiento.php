<?php

$config['db_cumplimiento'] = array(
    'host' => '10.1.131.170',
    'username' => 'UserAppCumpl',
    'password' => 'UserAppCumpl',
    'dbname' => 'Cumplimientos'
);

$config['msg_error_delete'] = '<div class="alert alert-error">No pudo eliminar los datos. Inténtalo otra vez.</div>';
$config['msg_success_delete'] = '<div class="alert alert-success">Los datos han sido eliminados correctamente.</div>';
$config['msg_success_save'] = '<div class="alert alert-success">Los datos han sido guardados correctamente.</div>';
$config['msg_error_save'] = '<div class="alert alert-error">No pudo gurdar los datos. Inténtalo ingresar otra vez.</div>';
$config['msg_error_app'] = '<div class="alert alert-error">Error de aplicación. Comunicase con el soporte tecnico.</div>';
$config['msg_error_delete_img'] = '<div class="alert alert-error">No pudo eliminar el imagen. Inténtalo otra vez.</div>';
$config['msg_permisos'] = '<div class="alert alert-error">Area restrengida.</div>';

$config['procesos_plazo_max'] = 12;
$config['procesos_plazo_min'] = 8;
$config['subprocesos_plazo_max'] = 12;
$config['subprocesos_plazo_min'] = 8;
$config['normas_plazo_max'] = 5;
$config['normas_plazo_min'] = 3;
$config['procedimientos_plazo_max'] = 12;
$config['procedimientos_plazo_min'] = 8;
$config['observaciones_plazo_max'] = 12;
$config['observaciones_plazo_min'] = 8;